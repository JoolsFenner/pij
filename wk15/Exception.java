import java.util.ArrayList;
//import java.util.List;

public class Exception{
	private void launch(int numbers){
		ArrayList<Integer> intList = new ArrayList<Integer>();
		// Some code here
		try {
			
			intList.add(numbers);
			intList.remove(0);
			//intList.remove(0);
			// more code here
		} catch (IndexOutOfBoundsException ex) {
			System.out.println("You are trying to access an element from an index location that doesn't exist");
			ex.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		int userInput = 0; //default
		try{
			userInput = Integer.parseInt(args[0]);
		} catch (NumberFormatException ex) {
			System.out.println("Invalid input, please enter an integer");
			ex.printStackTrace();
		}
		
		new Exception().launch(userInput);
	}
}