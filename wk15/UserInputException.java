import java.util.ArrayList;
//import java.util.List;

public class UserInputException{
	private void launch(){
		
		Integer numbersToTest = null;
		Integer currentNumber = null;
		int loopCount = 0; 
		int average = 0;
		
		do{
			System.out.print("How many numbers would you like to enter? ");
			numbersToTest = getNumber();
			
		}while (numbersToTest == null);
		
		do{
			System.out.println("Please a number");
			currentNumber = getNumber();
		
			if(currentNumber != null) {
				average += currentNumber;
				loopCount++;
			} 
			
			
		} while(loopCount < numbersToTest);
		
		System.out.println("The average is " + average/numbersToTest);	
	}
	
	private Integer getNumber(){
		String str;
		
		try{
			str = System.console().readLine();
			return Integer.parseInt(str);
		} catch(NumberFormatException ex){
			System.out.println("Error, please enter integers only");
			return null;
		}
			
	}
	
	public static void main(String[] args){
		
		new UserInputException().launch();
	}
}