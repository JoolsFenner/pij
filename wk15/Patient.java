public class Patient{
	String name;
	int age;
	public Patient(String name, int age){

		this.name = name;
		this.age = age;
		if(age < 0 || age > 130){
			throw new IllegalArgumentException("age " + age + " is outside of acceptable range");
		}
	}
		
	public void printDetails(){
		System.out.println(name + " " + age);
	}
}