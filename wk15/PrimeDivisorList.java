import java.util.ArrayList;
//import java.util.List;

public class PrimeDivisorList{
	MyArrayList<Integer> primeList = new MyArrayList<Integer>();

    private void createList(){
		
		Integer currentInput = null;
		boolean endInput = false;
		
		do{
			System.out.println();
			System.out.print("Enter prime numbers, 0 to finish: ");
			try{
				currentInput = getNumber();	
				testPrime(currentInput);
				if(currentInput!=0){
					primeList.add(currentInput);
				} else{
					endInput = true;
				}	
			} catch (NullPointerException ex){
				System.out.println("	Error Null Pointer");
			}	
		} while(!endInput);
		System.out.println(primeList.toString());
	}
	
	
	public Integer getNumber(){		
		try{
			String str = System.console().readLine();
			return Integer.parseInt(str);
				
		} catch(NumberFormatException ex){
			System.out.println("	Error, please enter integers only");
			return null;
		} 		
	}
	
	
	
	
	public boolean testPrime(int number){
		
		for(int i = 2; i < number; i++){
			if(number % i== 0){
				throw new IllegalArgumentException("Error. Number is a not a prime number");
			}
		}
		return true;
	}
	
	public static void main(String[] args){
		new PrimeDivisorList().createList();
	}
}
