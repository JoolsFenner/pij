import java.util.ArrayList;

public class Morepatients{
	
	private void launch(){
		ArrayList<Patient> patientList = new ArrayList<Patient>();
		
		patientList.add(new Patient("Julian", -1));
		patientList.add(new Patient("Andrea", 36));
		patientList.add(new Patient("David", 60));
		
		for(Patient next : patientList){
			next.printDetails();
		}
		
		
	}
	
	public static void main(String[] args){
		new Morepatients().launch();
	}
}