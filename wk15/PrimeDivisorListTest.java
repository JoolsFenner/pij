import org.junit.*;
import static org.junit.Assert.*;

/*
set classpath=C:/Julian/lib/junit-4.11.jar;C:/Julian/lib/hamcrest-core-1.3.jar; %classpath%

javac -cp .;C:/Julian/lib/junit-4.11.jar PrimeDivisorListTest.java

java org.junit.runner.JUnitCore PrimeDivisorListTest

*/	

public class PrimeDivisorListTest {

	@Test
	public void testPrime() {
		PrimeDivisorList pd = new PrimeDivisorList();
		Integer input = 3;
		boolean output = pd.testPrime(input);
		boolean expectedOut = true;
		assertEquals(output, expectedOut);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void nonPrimeTest() {
		PrimeDivisorList pd = new PrimeDivisorList();
		pd.testPrime(4);
	}
	



	
}