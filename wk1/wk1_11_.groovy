// 14 October comment
// need to check is same card entered


def cardRank = new Object[5]
def cardSuit = new Object[5]

boolean rankInputValidated = false
boolean suitInputValidated = false



//input and validate cards
for(i=0; i < 5; i++) {
	rankInputValidated = false
	suitInputValidated = false
	
	while(rankInputValidated != true) {
		print "Please enter suit of card " + (i+1) + ": "
		cardSuit[i] = System.console().readLine()
	
		if(cardSuit[i]== "spades" || cardSuit[i]== "hearts" || cardSuit[i]== "clubs" || cardSuit[i]== "diamonds"){
			rankInputValidated =true
			
		} else {
			println "Input valid.  Please enter spades, hearts, clubs or diamonds"
		}
	}
	
	while(suitInputValidated!= true){
		print "Please enter rank of card " + (i+1) + ": "
		cardRank[i] = System.console().readLine()
		
		if(cardRank[i]== "1" || cardRank[i]== "2" || cardRank[i]== "3" || cardRank[i]== "4" || cardRank[i]== "5"|| cardRank[i]== "6"|| cardRank[i]== "7"|| cardRank[i]== "8"|| cardRank[i]== "9"|| cardRank[i]== "10"|| cardRank[i]== "J"|| cardRank[i]== "Q" || cardRank[i]== "K"){
			suitInputValidated = true
			
			//convert input to integer
			if (cardRank[i]== "J"){ 
				cardRank[i]="11"
			}
			if (cardRank[i]== "Q"){ 
				cardRank[i]="12"
			}
			if (cardRank[i]== "K"){ 
				cardRank[i]="13"
			}
		
			cardRank[i]=Integer.parseInt(cardRank[i])
			
			
		} else{
			println "Input invalid.  Please enter 1-10 or J, Q, K"
		}
	}
	println ""
}

cardRankSort = cardRank.sort()
//println cardRankSort

//count suits
def cardSuitCount = [0,0,0,0]
boolean consecutive = true

for(i=0; i < 5; i++){
	if (cardSuit[i]=="spades"){
		cardSuitCount[0]++
	} else if(cardSuit[i]=="clubs"){
		cardSuitCount[1]++
	} else if (cardSuit[i]=="diamonds"){
		cardSuitCount[2]++
	} else{
		cardSuitCount[3]++
	}	
}
cardSuitCountSort = cardSuitCount.sort()

//count rank
def cardRankCount = [0,0,0,0,0,0,0,0,0,0,0,0,0]


for(i=0; i < 5; i++){
	if (cardRank[i]==1){
		cardRankCount[0]++	
	} else if(cardRank[i]==2){
		cardRankCount[1]++		
	} else if (cardRank[i]==3){
		cardRankCount[2]++		
	}else if (cardRank[i]==4){
		cardRankCount[3]++		
	} else if(cardRank[i]==5){
		cardRankCount[4]++	
	}else if(cardRank[i]==6){
		cardRankCount[5]++		
	}else if(cardRank[i]==7){
		cardRankCount[6]++		
	}else if(cardRank[i]==8){
		cardRankCount[7]++		
	}else if(cardRank[i]==9){
		cardRankCount[8]++		
	}else if(cardRank[i]==10){
		cardRankCount[9]++		
	}else if(cardRank[i]==11){
		cardRankCount[10]++	
	}else if(cardRank[i]==12){	
		cardRankCount[11]++	
	} else {
		cardRankCount[12]++	
	}
}

cardRankCountSort = cardRankCount.sort()

//check if consecutive
for(i=0; i < 4; i++){
	if (cardRankSort[i+1]-cardRankSort[i]!= 1 && consecutive == true){
		consecutive=false
	}
}

//println cardSuitCountSort
//println consecutive
//results
if (cardSuitCountSort[3] ==5 && consecutive==true){
		println "straight flush"
	} else if (cardRankCountSort[12] ==4){
		println "poker"
	} else if (cardRankCountSort[12]==3 && cardRankCountSort[11]==2) {
		println "full house"
	} else if (cardSuitCountSort[3]==5 && !consecutive) {
		println "flush"
	} else if (cardSuitCountSort[3] < 5 && consecutive){
		println "straight"
	} else if (cardRankCountSort[12]==3){
		println "three of a kind"
	} else if (cardRankCountSort[12]==2 && cardRankCountSort[11]==2){
		println "two pairs"
	} else if (cardRankCountSort[12]==2){
		println "pair"
	}else{
		println "nothing"
}
	
	

