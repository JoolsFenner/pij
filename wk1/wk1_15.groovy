int randomNumber = new Random().nextInt(600) + 1
print "Please try to guess the number: "
str = System.console().readLine();
int x = Integer.parseInt(str);

int attempts = 1

while(x!=randomNumber){
	
	if(x>randomNumber){
		println "too high"
	} else{
		println"too low"
	}

	print "Guess again: "
	str = System.console().readLine();
	x = Integer.parseInt(str)
	
	attempts++
	
}

println "Correct, the number is " +randomNumber +". You guessed it in " + attempts + " attempts."