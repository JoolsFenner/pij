public class AdultPerson implements Person{
	private String name;
	private int age;
	public AdultPerson (String name, int age){
		this.name = name;
		this.age=age;
	}
	
	public void move(int distance){
		//implementation code goes here
	}
	
	public String getName(){
		return name;
	}
	public int getAge(){
		return age;
	}
	
	public void findOld(){
		if(this.getAge() >=20 ){
			System.out.println("FOUND");
			return;
		}
		System.out.println("not found");
			//	findOld(person.getNext());
		
	}
}