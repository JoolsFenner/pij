public class Supermarket{
	
	public static void main(String[] args){
		Supermarket script = new Supermarket();
		script.launch();
	}
	
	public void addPerson(PersonQueue pq, String name, int age){
		pq.insert(name, age);
	}
	public void servePerson(PersonQueue pq){
		pq.retrieve();
	}
	
	
	private void launch(){
		//instantiateQueue
		PersonQueue tesco = new PersonQueueSupermarket();
		addPerson(tesco, "julian", 32);
		addPerson(tesco, "david", 67);	
		addPerson(tesco, "andrea", 36);
		addPerson(tesco, "graciela", 67);
		addPerson(tesco, "linda", 19);
		addPerson(tesco, "jon", 17);
		//tesco.printFirstLast();
		servePerson(tesco);
		/*
		servePerson(tesco);
		servePerson(tesco);
		servePerson(tesco);
		servePerson(tesco);
		*/

	}

}