public class PersonQueueSupermarket implements PersonQueue{
	private PersonQueueNode first;
	private PersonQueueNode last;
	
	public PersonQueueSupermarket(){
		first = null;
		last = null;
    }	
	
	public void insert(String name, int age){
		Person newPerson = new AdultPerson(name, age);
		PersonQueueNode newNode = new PersonQueueNode(newPerson);
		
		if(first==null){
			first = newNode;
			last = newNode;
			return;
		}
		
		last.setNext(newNode);
		last = newNode;	
	}
	
	public Person retrieve(){
		/*System.out.println(first.getPerson().getName() + " has been served and has left the queue");
		first=first.getNext();
		System.out.println(first.getPerson().getName() + " is now at the front of the queue");
		System.out.println();*/
		
		getOld(first.getPerson());
		//System.out.println(first.getPerson().getAge());
		//System.out.println(first.getNext().getPerson().getAge());
		//System.out.println(first.getNext());
		
		return first.getPerson();
	}
	
	private void getOld(){
	
	}

	public void printFirstLast(){
		System.out.println();
		System.out.println("First = " + first.getPerson().getName());
		System.out.println("Last = " + last.getPerson().getName());
		System.out.println("---------------------------");
		System.out.println();
	}		
}