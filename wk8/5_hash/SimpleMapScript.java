public class SimpleMapScript{
	public static void main(String[] args){
		SimpleMapScript s = new SimpleMapScript();
		s.launch();
	}
	
	private void launch(){
		SimpleMapTest joolsSimpleMap = new SimpleMapTest();
		joolsSimpleMap.put("a1");
		joolsSimpleMap.put("a1");
		joolsSimpleMap.put("a2");
		joolsSimpleMap.put("a2");
		joolsSimpleMap.put("a3");
		joolsSimpleMap.put("a2");
		joolsSimpleMap.put("a4");
		joolsSimpleMap.put("a2");
		joolsSimpleMap.put("a5");
		joolsSimpleMap.put("a1");
		joolsSimpleMap.put("15");
	
		
		joolsSimpleMap.printList();
		System.out.println();
		System.out.println(joolsSimpleMap.get(11));
		//System.out.println(joolsSimpleMap.get(1111));
		System.out.println();
		joolsSimpleMap.remove(11);
		joolsSimpleMap.remove(12);
		joolsSimpleMap.remove(13);
	
		
		joolsSimpleMap.printList();
		
		
	}
}