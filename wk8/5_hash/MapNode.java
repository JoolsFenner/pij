/**
 * A node in a dynamic singly-linked list of Strings
 */
public class MapNode {
    //NEED TO MAKE ATTRIBUTES PRIVATE
	public String userInput;
	public int hashKey;
    public MapNode next;

    public MapNode(String str) {
	  this.userInput = str;
	  this.hashKey = HashUtilities.shortHash(str);
	  next = null;
    }

    public void setNext(MapNode node) {
		node.next = this;		
    }

    public MapNode getNext() {
	  return next;		
    }
	
	public int getHashKey() {
	  return hashKey;		
    }
	
}