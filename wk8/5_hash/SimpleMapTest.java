public class SimpleMapTest implements SimpleMap{
	private MapNode head;
	public SimpleMapTest(){
		this.head=null;
	}
	
	public void put(String name){
		MapNode newNode = new MapNode(name);
		
		if(this.head==null){
			this.head = newNode;
			return;
		}
		put(this.head, newNode);
	}
	private void put(MapNode currentNode, MapNode newNode){
		
		if (keyClash(newNode)){
			System.out.println("CLASH " + newNode.userInput + " not added to list.");
			return;
		}
	
		if(currentNode.next==null){
			currentNode.next=newNode;
		} else{
			put(currentNode.next, newNode);
		}
			
	}
	
	public String get(int key){
		String result = get(this.head, key);
		return result;
	}
	private String get(MapNode currentNode, int key){
		if(currentNode.hashKey == key){
			return currentNode.userInput;
		} else if(currentNode.next==null){
			return "not found";
		}else{
			return get(currentNode.next, key);
		}	
	}	
			
	public void remove(int key){
		if(this.head.hashKey==key){
			head = head.next;
		} else{
			remove(this.head, key);
		}
	}
	private void remove(MapNode currentNode,int key){
		if(currentNode.next == null){
			System.out.println("hashkey " + key + " does not exist, cannot delete");
			return;
		}
		
		if(currentNode.next.hashKey == key){
			if(currentNode.next == null){
				currentNode=null;
				return;
			} else{
				currentNode.next = currentNode.next.next;
				return;
			}
		} 
		remove (currentNode.next, key);	
	}
	
	public boolean isEmpty(){
		return true;
	}
	
	public void printList(){
		printList(this.head);
	}
	private void printList(MapNode currentNode){
		
		System.out.println("Input: " + currentNode.userInput);
		System.out.println("Hash key: " + currentNode.hashKey);
		System.out.println();
		
		if(currentNode.next != null){
			printList(currentNode.next);
		}	
	}
	
	public boolean keyClash(MapNode newNode){
		MapNode aux = head; 
		boolean endLoop=false;
		do{
			if(aux.hashKey == newNode.hashKey){
				return true;
			}
			if(aux.next !=null){
				aux = aux.next;
			} else{
				endLoop = true;
			}
		}while(!endLoop);

		return false;	
	}
	
	
}