//PROBLEM WITH THIS HASH FUNCTION IS THAT IT TENDS TO RETURN LOW VALUES

public class HashUtilities{

	public static int shortHash(String x){
		int key;
		if(isInteger(x)){
			//convert int to hash
			int y =Integer.parseInt(x);
			key = createKey(y);
			return key;
		}
		//convert string to number 
		int alphaStringSum = 0;
		for(int i=0; i < x.length(); i++){
			alphaStringSum += Character.getNumericValue(x.charAt(i)); 
		}
		key = createKey(alphaStringSum);
		return key;
	}
	
	private static boolean isInteger(String x) {
	//check if input string is contains only 0-9
		int size = x.length();

		for (int i = 0; i < size; i++) {
			if (!Character.isDigit(x.charAt(i)) && !(x.charAt(i)== '-')) {
				return false;
			}
			
		}
		return true;
	}
	
	private static int createKey(int x){
	//convert int to hash
			int y = Math.abs(x);
			y = y%1000;
			return y;
	}
}
	
