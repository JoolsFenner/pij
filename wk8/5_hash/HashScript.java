public class HashScript{
	public static void main (String[] args){
		HashScript hs = new HashScript();
		hs.launch();
	}
	
	private void launch(){
		
		String input;
		do{
			System.out.print("Enter a number to convert to hashkey: ");
			input =	System.console().readLine();
			System.out.println("Your input " + input + " has been converted to "+ HashUtilities.shortHash(input));
			System.out.println();
		}while(!input.equals("0"));		
	
	}
}
