public class KidPerson implements Person {
	private int position;
	private int energy;
	//constructor
	public KidPerson(int position, int energy){
		this.position = position;
		this.energy = energy;
	}
	
	
	/**
	* Move a distance in a straight line, given in meters
	*/
	public void move(int distance) {
		crawl(distance);
	}
	/**
	* Say something
	*/
	public void say(String message) {
		System.out.println(message);
	}
	private void crawl(int distance) {
		for (int i = 0; i < distance; i++) {
			position++;
			System.out.println(this.position);
		}
	}
	
	
}