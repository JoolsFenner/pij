public class AdultPerson implements Person {
	private int position;
	private int energy;
	boolean injured;
	//constructor
	public AdultPerson(int position, int energy, boolean injured){
		this.position = position;
		this.energy = energy;
		this.injured = injured;
	}
	
	/**
	* Move a distance in a straight line, given in meters
	*/
	public void move(int distance) {
		if(this.injured==true){
			System.out.println("This adult is injured, he can only move half the distance of " + distance + " you have requested");
			distance = distance/2;
		}
		
		for (int i = 0; i < distance; i++) {
			position++;
			System.out.println(this.position);
		}
	}
	/**
	* Say something
	*/
	public void say(String message) {
		System.out.println(message);
	}
	private void crawl(int distance) {
		for (int i = 0; i < distance; i++) {
			position++;
			System.out.println(this.position);
		}
	}
	
	
}