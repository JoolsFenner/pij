public class PersonScript{

	
	
	public static void main(String[] args) {
		PersonScript p = new PersonScript();
		p.launch();
			
	}
	
	public void launch(){
		System.out.println("Create KidPerson Object");
		Person joolsKid = new KidPerson(2,3);
		joolsKid.move(5);
		joolsKid.say("hello");
		
		System.out.println();
		System.out.println();
		
		System.out.println("Create AdultPerson Object");
		Person brianAdult = new AdultPerson(4,3, true);
		brianAdult.move(6);
	
	}
}