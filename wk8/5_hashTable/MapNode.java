/**
 * A node in a dynamic singly-linked list of Strings
 */
public class MapNode {
    //NEED TO MAKE ATTRIBUTES PRIVATE
	public String userInput;
	public Integer listKey;
    public MapNode next;

    public MapNode(Integer key, String str) {
	  this.userInput = str;
	  this.listKey = key;
	  next = null;
    }

    public void setNext(MapNode node) {
		node.next = this;		
    }

    public MapNode getNext() {
	  return next;		
    }
	
	public int getKey() {
	  return listKey;	
    }
	
}