public class SimpleMapInstance /*implements Simplemap*/{
	public SimpleMapList[] hashTable;
	public SimpleMapInstance(){
		hashTable = new SimpleMapList[1000];
	}


	
	public void put(Integer key, String name){
		int tempHash = HashUtilities.shortHash(key);
		
		for(int i=0; i < hashTable.length; i++){
			if(i == (tempHash - 1)){
				
				if(hashTable[i]==null){
					SimpleMapList tempList = new SimpleMapList();
					tempList.put(key, name);
					hashTable[i] = tempList;
				} else{
					hashTable[i].put(key, name);
				}
				
			}
		}
	}
	
	public void printTable(){
		for(int i=0; i < hashTable.length; i++){
			if(hashTable[i] !=null){
				System.out.println("_____________");
				System.out.println("HASH KEY " + i);
				hashTable[i].printList();
				
			}
		}
	}
	
	
	public /*String[]*/ String get(Integer key){
		Integer tempHash = HashUtilities.shortHash(key) -1;
		return hashTable[tempHash].get(key);
	}
	
	/*
	//INCOMPLETE IMPLEMENTATION
	
	
	public void remove(int key, String name){
	
	}
	
	public boolean isEmpty(){
	
	}*/
}