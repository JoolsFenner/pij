public class SimpleMapList{
	private MapNode head;
	public SimpleMapList(){
		this.head=null;
	}
	//ADD
	public void put(Integer key, String name){
		MapNode newNode = new MapNode(key, name);
		
		if(this.head==null){
			this.head = newNode;
			return;
		}
		put(this.head, newNode);
	}
	private void put(MapNode currentNode, MapNode newNode){
		
		if(currentNode.next==null){
			currentNode.next=newNode;
		} else{
			put(currentNode.next, newNode);
		}
			
	}
	
	//GET
	public String get(Integer key){
		String result = get(this.head, key);
		return result;
	}
	private String get(MapNode currentNode, Integer key){
		String tempStr = "";
		MapNode aux = currentNode;
		boolean stop = false;
		
		if(aux.listKey == key){
			tempStr += aux.userInput + ",";
		}
		do{
			aux = aux.next;
			
			if(aux.listKey == key){
				tempStr += aux.userInput + ",";
			}

			if(aux.next == null){
				stop = true;
			}
		} while(stop != true);
		
		return tempStr;
		/*if(currentNode.listKey == key){
			return currentNode.userInput;
		} else if(currentNode.next==null){
			return "not found";
		}else{
			return get(currentNode.next, key);
		}*/	
	}	
	
	//REMOVE	
	public void remove(Integer key){
		if(this.head.listKey==key){
			head = head.next;
		} else{
			remove(this.head, key);
		}
	}
	private void remove(MapNode currentNode,Integer key){
		if(currentNode.next == null){
			System.out.println("listKey " + key + " does not exist, cannot delete");
			return;
		}
		
		if(currentNode.next.listKey == key){
			if(currentNode.next == null){
				currentNode=null;
				return;
			} else{
				currentNode.next = currentNode.next.next;
				return;
			}
		} 
		remove (currentNode.next, key);	
	}
	
	//NEEDS IMPLEMENTING
	public boolean isEmpty(){
		return true;
	}
	
	//PRINT LIST
	public void printList(){
		printList(this.head);
	}
	private void printList(MapNode currentNode){
		
		System.out.println("Input: " + currentNode.userInput);
		System.out.println("key: " + currentNode.listKey);
		System.out.println();
		
		if(currentNode.next != null){
			printList(currentNode.next);
		}	
	}
		
}