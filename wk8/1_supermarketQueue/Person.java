/**
* A person is defined by movement (as opposed to plants)
* and by speech (as opposed to animals).
*/
public interface Person {
	/**
	* Move a distance in a straight line, given in meters.
	*/
	void move(int distance);
	/**
	* Get's name of Person
	*/
	String getName();
}