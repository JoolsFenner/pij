public class SuperMarket{
	
	public static void main(String[] args){
		SuperMarket script = new SuperMarket();
		script.launch();
	}
	
	public void addPerson(PersonQueue pq, String name){
		pq.insert(name);
	}
	public void servePerson(PersonQueue pq){
		pq.retrieve();
	}
	
	
	private void launch(){
		//instantiateQueue
		PersonQueue tesco = new PersonQueueSupermarket();
		addPerson(tesco, "julian");
		addPerson(tesco, "david");	
		addPerson(tesco, "andrea");
		addPerson(tesco, "graciela");
		addPerson(tesco, "linda");
		addPerson(tesco, "jon");
		tesco.printFirstLast();
		
		/*
		servePerson(tesco);
		servePerson(tesco);
		servePerson(tesco);
		servePerson(tesco);
		servePerson(tesco);
		*/

	}

}