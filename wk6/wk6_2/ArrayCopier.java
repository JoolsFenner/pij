public class ArrayCopier {
	/*public int[] src;
	public int[] dst;
	
	public ArrayCopier(int[] src, int[] dst){
		this.src = src;
		this.dst = dst;
	}
	*/
	public static int[] copy(int[] src, int[] dst){
			for (int i=0; i < src.length; i++){
				dst[i] = src[i];
			}
			
			return dst;
	}
}