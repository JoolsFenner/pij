public class Calculator {
	private int x;
	private int y;
	public Calculator(int x, int y){
		this.x = x;
		this.y = y;
	
	}
	
	public int add(){
		return (this.x + this.y);
	}
	public int subtract(){
		return (this.x - this.y);
	}
	public int multiply(){
		return (this.x * this.y);
	}
	public double divide(){
		double xx = (double) this.x;
		double yy = (double) this.y;
		
		return (xx / yy);
	}
	public int modulus(){
		return (this.x % this.y);
	}
	
	

}
