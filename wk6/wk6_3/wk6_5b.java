import java.util.Scanner;
public class wk6_5b
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
//using Matrix class and MatrixChecker together

Matrix jools = new Matrix(3,3);
jools.prettyPrint();

jools.setElement(0,0,1);
jools.setElement(0,1,2);
jools.setElement(0,2,4);
jools.setElement(1,0,2);
jools.setElement(1,1,3);
jools.setElement(1,2,5);
jools.setElement(2,0,4);
jools.setElement(2,1,5);
jools.setElement(2,2,6);

jools.prettyPrint();
System.out.print("array symmetry: ");
jools.isSym();
}}

