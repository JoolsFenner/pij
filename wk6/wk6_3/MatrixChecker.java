public class MatrixChecker {
	
	public static boolean isSymmetrical(int[] oneD){
		for (int i=0; i < oneD.length; i++){
			if(oneD[i] != oneD[oneD.length -(i+1)]){
				return false;
			}
		}
			return true;
	}
			
	public static boolean isSymmetrical(int[][] twoD){
		for (int i=0; i < twoD.length; i++){
			for (int j=0; j < twoD[0].length; j++){
				if (twoD[i][j] != twoD[j][i]){
					return false;
				}
			}
		}
		return true;
	}
	
	public static boolean isTriangular(int[][] twoD){
		for (int i=0; i < twoD.length; i++){
			for (int j=0; j < twoD[0].length; j++){
				if(i > j && twoD[i][j]!=0){
					return false;
				}
			}
		}
		return true;
	}
			
			
}