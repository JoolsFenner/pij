public class Matrix{
	private int[][] array2d;
	//constructor
	public Matrix(int rowDimension, int columnDimension){
		this.array2d = new int[rowDimension][columnDimension];
		
		for(int row = 0; row < rowDimension; row++){
			for(int column = 0; column < columnDimension; column++){
				this.array2d[row][column] =1;
			}
		}		
	}
	//get method
	public void getElement(int row, int column){
		System.out.println(this.array2d[row][column]);
	}
	
	//set methods
	public void setElement(int row, int column, int newValue){
		if(row >= this.array2d.length || column >= this.array2d[0].length){
			System.out.println("input invalid");
			return;	
		} 
		
		this.array2d[row][column] = newValue;

	}
	
	public void setRow(int row,int newValue){
		
		
		for(int i=0; i<this.array2d[row].length; i++){
			this.array2d[row][i]=newValue;
		}
	}
	
	public void setColumn(int column, String newValue){	
		String[] newValueSplit = newValue.split(",");
		if(newValueSplit.length != this.array2d.length){
			System.out.println("input invalid");
			return;
		}
		
		for(int i=0; i < this.array2d.length; i++){
		
			this.array2d[i][column]= Integer.parseInt(newValueSplit[i]);
		}
	}
	
	//print methods
	public void toStringPrint(){
		System.out.print("[");
		for(int row=0; row < this.array2d.length; row++){	
			for(int column=0; column < this.array2d[0].length; column++){
				System.out.print(this.array2d[row][column]);
				if (column !=  this.array2d[0].length-1){
					System.out.print(",");
				}
			}
			if(row != this.array2d.length-1){
				System.out.print(";");
			}
		}
		System.out.println("]");
		System.out.println("");
	}
	
	public void prettyPrint(){
		for(int row=0; row < this.array2d.length; row++){	
			for(int column=0; column < this.array2d[0].length; column++){
				System.out.print(this.array2d[row][column] + "\t");
			}
			
			System.out.println("");	
		}
		System.out.println("");
	}
	//check methods using MatrixChecker class
	public void isSym(){
		//MatrixChecker matrixCheckInstance = new MatrixChecker();
		//System.out.println(matrixCheckInstance.isSymmetrical(this.array2d));
		System.out.println(MatrixChecker.isSymmetrical(this.array2d));
	
	}
	
} 