import java.util.Scanner;
public class wk6_5
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
int[] oneDtest  = {2,3,5,3,1};
int[][] twoDtest = {{1,2,4},{2,3,5},{4,5,6}};
int[][] triangleTest = {{1,2,3},{0,5,6},{0,0,9}};

System.out.println("one d " + MatrixChecker.isSymmetrical(oneDtest));
System.out.println("two d " + MatrixChecker.isSymmetrical(twoDtest));
System.out.println("two d " + MatrixChecker.isTriangular(triangleTest));
System.out.println("");
}}

