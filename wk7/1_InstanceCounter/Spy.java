public class Spy{
	private static int spyCount=0;
	private int spyId;
	public Spy(int spyId){
		this.spyId = spyId;
		this.spyCount++;
		System.out.println("Spy no " + this.spyId + " has been created. There are " + this.spyCount + " spies.");
	}
	public void die(){
		this.spyCount--;
		System.out.println("Spy " + this.spyId + " has been detected and eliminated.  The number of spies is now " + this.spyCount);
	}
	
	public static void main(String[] args){
		int id1 =  Integer.parseInt(args[0]);
		int id2 =  Integer.parseInt(args[1]);
		int id3 =  Integer.parseInt(args[2]);
		
		Spy spy1 = new Spy(id1);
		Spy spy2 = new Spy(id2);
		Spy spy3 = new Spy(id3);
		System.out.println("");
		spy2.die();
		System.out.println("");
		Spy spy4 = new Spy(4);
	
	}
	

}