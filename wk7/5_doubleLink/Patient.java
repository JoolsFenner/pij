public class Patient {
	private String name;
	private int age;
	private String illness;
	public Patient nextPatient;
	public Patient previousPatient;
	//constructor;
	public Patient(String name, int age, String illness) {
		this.name = name;
		this.age = age;
		this.illness = illness;
		this.nextPatient = null;
		this.previousPatient = null;
	}
	
	public void addPatient(Patient newPatient) {
		if (this.nextPatient == null) {
		// this means this is the last patient in the list
			this.nextPatient = newPatient;
			this.nextPatient.previousPatient = this;
		} else {
			this.nextPatient.addPatient(newPatient);
		}
	}
	
	public void getAllPatientsDetails(){
		System.out.println("name = " + this.name);
		System.out.println("age = " + this.age);
		System.out.println("illness = " + this.illness);
		System.out.println("this pointer = " + this);
		System.out.println("next = " + this.nextPatient);
		System.out.println("previous = " + this.previousPatient);
		System.out.println("");
		if (this.nextPatient != null) {
			this.nextPatient.getAllPatientsDetails();
		}
	}
	
	public boolean deletePatient(HospitalManager hm, Patient patient) {
		
		if(hm.get().equals(patient)){
		//if deleting first element
			patient.nextPatient.previousPatient = null;
			hm.changeHead();
			//patient.previousPatient = null;
			System.out.println("First element " + patient.name + " deleted succesfully.");
			return true;
		}
		
		if (this.nextPatient == null) {
		// patient to remove was not found
			return false;
		} else if (this.nextPatient.name.equals(patient.name)) {
		// We found it! It is the next one!
		// Now link this patient to the one after the next
			System.out.println(patient.name + " deleted succesfully.");
			//correct previousPatient for element after one being deleted.  Note if this statement comes after statement below it should be written "nextPatient.previousPatient = this;" as garbage collection will have already deleted the 'middle' element
			nextPatient.nextPatient.previousPatient = this;
			//changes pointer to next, i.e target, element to the one after.
			this.nextPatient = nextPatient.nextPatient;
			
			
		return true;
		} else {
			return this.nextPatient.deletePatient(hm, patient);
		}	
	}
	
	public void listLength(){
		int size = 	0;
		Patient currentPatient = this;
		//Patient currentPatient = this.nextPatient;
		
		do{
			size++;
			currentPatient = currentPatient.nextPatient;
		} while(currentPatient != null);
		
		System.out.println("The list is " + size + " elements long.");
	}
	
}