public class HospitalManager {
	private Patient patientListStart = null;
	
	public Patient get(){
		return this.patientListStart;
	}
	public void changeHead(){
		this.patientListStart = this.patientListStart.nextPatient;
	}
	public static void main(String[] args) {
		
		HospitalManager hm = new HospitalManager();
		hm.launch();
	}
	
	public void launch(){
		//add first patient
		
		
		
		
		Patient firstPatient = new Patient("John", 33, "Tuberculosis");
		patientListStart = firstPatient;
		
		Patient secondPatient = new Patient("Mary", 66, "Meningitis");
		patientListStart.addPatient(secondPatient);
		
		Patient thirdPatient = new Patient("Patient3", 1, "Illness3");
		patientListStart.addPatient(thirdPatient);
		
		Patient fourthPatient = new Patient("Patient4", 5, "Illness4");
		patientListStart.addPatient(fourthPatient);
		
		Patient fifthPatient = new Patient("Patient5", 44, "Illness5");
		patientListStart.addPatient(fifthPatient);
		
		Patient sixthPatient = new Patient("Patient6", 34, "Illness6");
		patientListStart.addPatient(sixthPatient);
		
		Patient seventhPatient = new Patient("Patient7", 90, "Illness7");
		patientListStart.addPatient(seventhPatient);
		
		Patient eigthPatient = new Patient("Patient8", 102, "Illness8");
		patientListStart.addPatient(eigthPatient);
		
		Patient ninthPatient = new Patient("Patient9", 2, "Illness9");
		patientListStart.addPatient(ninthPatient);
		
		Patient tenthPatient = new Patient("Patient10", 35, "Illness10");
		patientListStart.addPatient(tenthPatient);
		
		
		
		//TEST OUTPUT		
		System.out.println("--------------initial list-----------------------");
		patientListStart.getAllPatientsDetails();
		patientListStart.listLength();
	
		System.out.println("--------------deleting second element-----------------------");
		patientListStart.deletePatient(this, secondPatient);
		patientListStart.getAllPatientsDetails();
		patientListStart.listLength();
		
		System.out.println("--------------deleting first element-----------------------");
		patientListStart.deletePatient(this, firstPatient);
		patientListStart.getAllPatientsDetails();
		patientListStart.listLength();	
	}
	
}