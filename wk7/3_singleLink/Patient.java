public class Patient {
	private String name;
	private int age;
	private String illness;
	public Patient nextPatient;
	//constructor;
	public Patient(String name, int age, String illness) {
		this.name = name;
		this.age = age;
		this.illness = illness;
		this.nextPatient = null;
	}
	
	public void addPatient(Patient newPatient) {
		if (this.nextPatient == null) {
		// this means this is the last patient in the list
			this.nextPatient = newPatient;
		} else {
			this.nextPatient.addPatient(newPatient);
		}
	}
	
	public void getAllPatientsDetails(){
		System.out.println(this.name);
		System.out.println(this.age);
		System.out.println(this.illness);
		System.out.println("");
		if (this.nextPatient != null) {
			this.nextPatient.getAllPatientsDetails();
		}
	}
	
	public boolean deletePatient(HospitalManager hm, Patient patient) {
		
		if(hm.get().equals(patient)){
			hm.changeHead();
			System.out.println("First element " + patient.name + " deleted succesfully.");
			return true;
		}
		
		if (this.nextPatient == null) {
		// patient to remove was not found
			return false;
		} else if (this.nextPatient.name.equals(patient.name)) {
		// We found it! It is the next one!
		// Now link this patient to the one after the next
			System.out.println(patient.name + " deleted succesfully.");
			this.nextPatient = nextPatient.nextPatient;
		return true;
		} else {
			return this.nextPatient.deletePatient(hm, patient);
		}
		
	}
	
	public void listLength(){
		int size = 	0;
		Patient currentPatient = this;
		//Patient currentPatient = this.nextPatient;
		
		do{
			size++;
			currentPatient = currentPatient.nextPatient;
		} while(currentPatient != null);
		
		
		System.out.println("The list is " + size + " elements long.");
		
		
	}
	
	

	
	
}