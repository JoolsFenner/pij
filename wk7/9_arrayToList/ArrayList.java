public class ArrayList {
	ListNode headNode = null;
			
	public static void main(String[] args){
		ArrayList a = new ArrayList();
		a.launch();
	}
		
	private void launch(){
		System.out.println("test 1");
		ArrayList a = new ArrayList();
		int[] jools = {4,3,90,5,4,6,-1};
		a.headNode = ListUtilities.arrayToList(true, jools);
		ListUtilities.printlist(a.headNode);
		
		System.out.println();
		
		System.out.println("test 2");
		ArrayList b = new ArrayList();
		int[] jools2 = {5,3,5,5,6};
		b.headNode = ListUtilities.arrayToList(false, jools2);
		ListUtilities.printlist(b.headNode);
		
	}
}