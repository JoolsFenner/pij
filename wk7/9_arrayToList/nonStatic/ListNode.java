public class ListNode{
	private int n;
	private ListNode next;
	
	public ListNode(int n){
		this.n = n;
		this.next = null;
	}
	public void add(ListNode a){
		if(this.next == null){
			this.next = a;
		} else{
			this.next.add(a);
		}
	}
	
	public void printlist(){
		System.out.println(this);
		System.out.println(this.n);
		System.out.println(this.next);
		System.out.println();
		
		if(this.next != null){
			this.next.printlist();
		}
	}
	public ListNode getNext(){
		return next;
	}
	
	public void setNext(ListNode aux){
		this.next = aux;
	}
}