public class ListNode{
	public int n;
	public ListNode next;
	//constructor
	public ListNode(int n){
		this.n = n;
		this.next = null;
	}
	//add node
	public void add(ListNode a){
		if(this.next == null){
			this.next = a;
		} else{
			this.next.add(a);
		}
	}		
	//getters
	public ListNode getNext(){
		return next;
	}
	public int getValue(){
		return n;
	}
	//setter
	public void setNext(ListNode aux){
		this.next = aux;
	}
	//setter (sort)
	public void setNextSort(ListNode aux){
		aux.next = this.next;
		this.next = aux;
	}
			
}