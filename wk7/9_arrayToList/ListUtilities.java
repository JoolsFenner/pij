public class ListUtilities{
	private ListNode first;
	public ListUtilities(){
		first = null;
	}
	public void addToList(boolean sort, int x){
		ListNode newNumber = new ListNode(x);
		
		if(first == null){
		//add first element to list
			first = newNumber;	
			return;
		} 
		
		if(first.getValue() > x && sort == true){
		//if first element has higher value than new limit when sort is true
			newNumber.next = this.first;
			this.first = newNumber;
			return;
		}
		
		ListNode aux = first; 
	    
		while (aux.getNext() != null) {
			if(aux.getNext().getValue() > x && sort == true){
			//places value in correct position is sort is true
				aux.setNextSort(newNumber);
				return;
			}
			aux = aux.getNext();
		}
		
		aux.setNext(newNumber);
	
	}
	
	public static void printlist(ListNode x){
		System.out.println("Pointer = " + x);
		System.out.println("Value of n = " + x.n);
		System.out.println("Pointer next = "+ x.next);
		
		System.out.println();
		
		if(x.next != null){
			printlist(x.next);
		}
	}
	/** NOT IMPLEMENTED YET, SEE NOTES. wk7_11
	public static void bubbleSort(ListNode x){
		if(x.getValue() > x.next.getValue()){
			x.setNextSort(x);
		}
	
	}
	*/
	
	public static ListNode arrayToList(boolean sort, int [] arrayToConvert){
			ListUtilities listUtilitiesInstance = new ListUtilities();
			for(int i=0; i < arrayToConvert.length; i++){
				
				if(sort==true){
					listUtilitiesInstance.addToList(sort, arrayToConvert[i]);
				} else{
					listUtilitiesInstance.addToList(sort, arrayToConvert[i]);
				}
			}
		
		return listUtilitiesInstance.first;		
		
	}
}