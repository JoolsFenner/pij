public class ReadMarks{
	private int distinction;
	private int pass;
	private int fail;
	private int invalid;
	public ReadMarks(){
		this.distinction = 0;
		this.pass = 0;
		this.fail = 0;
		this.invalid = 0;
	}
	
	

	public static void main(String[] args){
		ReadMarks readMarksInstance = new ReadMarks();
		readMarksInstance.launch();	
	}
	
	private void launch() {
		System.out.println("enter student marks between 1 and 100. -1 to finish: ");
		int result=0;
		while (result != -1){
			result = Integer.parseInt(System.console().readLine());
			allocateGrade(result);
		} 
		
		System.out.println("distinction " + this.distinction);
		System.out.println("pass " + this.pass);
		System.out.println("fail " + this.fail);
		System.out.println("invalid " + this.invalid);
	}
	
	public void allocateGrade(int result){
		if(result == -1){
			return;
		} else if (result <=100 && result >= 70){
			this.distinction++;
		} else if (result >= 50){
			this.pass++;
		} else if(result >=0){
			this.fail++;
		} else{
			this.invalid++;
		}
	}

}