public class HospitalManager {
	private Patient patientListStart = null;
	
	public Patient get(){
		return this.patientListStart;
	}
	public void changeHead(){
		this.patientListStart = this.patientListStart.nextPatient;
	}
	public static void main(String[] args) {
		
		HospitalManager hm = new HospitalManager();
		hm.launch();
	}
	
	public void launch(){
		//add first patient
		
		Patient firstPatient = new Patient("John", 33, "Tuberculosis");
		patientListStart = firstPatient;
		firstPatient.nextPatient = this.get();
		
		Patient secondPatient = new Patient("Mary", 66, "Meningitis");
		patientListStart.addPatient(this, secondPatient);
		
		Patient thirdPatient = new Patient("Patient3", 1, "Illness3");
		patientListStart.addPatient(this,thirdPatient);
		
		Patient fourthPatient = new Patient("Patient4", 5, "Illness4");
		patientListStart.addPatient(this, fourthPatient);
		
		Patient fifthPatient = new Patient("Patient5", 44, "Illness5");
		patientListStart.addPatient(this, fifthPatient);
		/*
		Patient sixthPatient = new Patient("Patient6", 34, "Illness6");
		patientListStart.addPatient(this, sixthPatient);
		
		Patient seventhPatient = new Patient("Patient7", 90, "Illness7");
		patientListStart.addPatient(this, seventhPatient);
		
		Patient eigthPatient = new Patient("Patient8", 102, "Illness8");
		patientListStart.addPatient(this, eigthPatient);
		
		Patient ninthPatient = new Patient("Patient9", 2, "Illness9");
		patientListStart.addPatient(this, ninthPatient);
		
		Patient tenthPatient = new Patient("Patient10", 35, "Illness10");
		patientListStart.addPatient(this, tenthPatient);
		*/
		
		
		//TEST OUTPUT		
		System.out.println("--------------initial list-----------------------");
		patientListStart.getAllPatientsDetails(this);
		//patientListStart.listLength();
	
	}
	
}