import java.util.Scanner;
public class wk3_14
{
	public static void main(String[] args)
	{
		Scanner scanner=new Scanner(System.in);
Point a = new Point();
a.x = 1;
a.y = 5;

Point b = new Point();
b.x = 3;
b.y = 1;

Rectangle joolsRectangle = new Rectangle();
joolsRectangle.upLeft = a;
joolsRectangle.downRight = b;

Point testPoint = new Point();
testPoint.x = 2;
testPoint.y = 5;

if(testPoint.x > joolsRectangle.downRight.x ||  testPoint.x < joolsRectangle.upLeft.x || testPoint.y < joolsRectangle.downRight.y || testPoint.y > joolsRectangle.upLeft.y){
	System.out.println("Test point is outside of rectangle");
} else{
	System.out.println("Test point is inside rectangle");
}

}}

class Point{
	double x;
	double y;
}

class Rectangle {
	Point upLeft;
	Point downRight;
}
