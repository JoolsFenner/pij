import java.util.Scanner;
public class wk3_8
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
System.out.print("Please enter a word: ");
String str = scanner.next();

int endChar;
boolean isPalindrome = true;

for(int i=0; i < (str.length() / 2); i++){
	//deals with words with odd numbers of letters e.g. kayak as it iterates to 'length / 2'
	
	endChar = (str.length() - (i+1));
	
	if (str.charAt(i)!= str.charAt(endChar) && isPalindrome){
		isPalindrome=false;
	}
	
}

System.out.println(str + " is palidrome? = " + isPalindrome);
}}

