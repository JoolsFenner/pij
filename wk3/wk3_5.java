import java.util.Scanner;
public class wk3_5
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
/*Write a program that reads some text from the user and then says how many letters �e� are there  in that text.Then modify the program so that it reads the text from the user and then asks for 
 a letter. The program shouldthen say how many times you can find the letter in the text.*/

System.out.print("Please enter a word: ");
String str = scanner.next();
int stringLength = str.length();

char charFindLcase = 'e';
char charFindUcase = 'E';
int charFind = 0;


for(int i=0; i < stringLength; i++){
	if (str.charAt(i)==charFindLcase || str.charAt(i)==charFindUcase){
		charFind++;
	}
}

System.out.print("Your word " + str + " contains " + charFind + " instances " + "of the letter e.");
}}

