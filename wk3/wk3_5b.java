import java.util.Scanner;
public class wk3_5b
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
/*Write a program that reads some text from the user and then says how many letters �e� are there  in that text.Then modify the program so that it reads the text from the user and then asks for 
 a letter. The program shouldthen say how many times you can find the letter in the text.*/

System.out.print("Please enter a word: ");
String str = scanner.next();
int stringLength = str.length();

System.out.print("Please enter a character you want to count in your chosen word: ");
String charFindString = scanner.next();
char charFind =charFindString.charAt(0);
System.out.println("");
int charFindCount = 0;


for(int i=0; i < stringLength; i++){
	if (str.charAt(i)==charFind){
		charFindCount++;
	}
}

System.out.print("Your word " + str + " contains " + charFindCount + " instances " + "of the letter " + charFind +".");
System.out.println("");
}}

