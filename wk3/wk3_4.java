import java.util.Scanner;
public class wk3_4
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
/*Write a program that reads some text from the user and then writes the same text on screen, but each letter on a
different line.
Now modify your program to write each word (as defined by spaces) rather than letter on a different line.
*/

System.out.print("Please enter a word: ");
String str = scanner.next();
int stringLength = str.length();

for(int i=0; i < stringLength; i++){
	System.out.println(str.charAt(i));
}
}}

