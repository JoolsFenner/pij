import java.util.Scanner;
public class wk3_6
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
System.out.println("");
System.out.print("Please enter a word or sentence: ");
String longStr = scanner.next();
int longStrLength = longStr.length();

System.out.print("Please enter a short string : ");
String shortStr = scanner.next();
int shortStrLength = shortStr.length();

int iCheck=0;
int charMatchCount=0;
int strMatchCount=0;

for(int i=0; i <= (longStrLength-shortStrLength); i++){
	
	iCheck = i;
	charMatchCount = 0;
	
	for(int j=0; j < shortStrLength; j++){
		if(longStr.charAt(iCheck)==shortStr.charAt(j)){
			charMatchCount++;
		}
		iCheck++;
	}
	
	if(charMatchCount==shortStrLength){
		strMatchCount++;
	}
	
}
System.out.println("");
System.out.print("The string '" + shortStr + "' appears " + strMatchCount + " times in the sentence '" + longStr + "'.");
}}

