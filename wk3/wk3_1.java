import java.util.Scanner;
public class wk3_1
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
/*Write a program that reads two numbers from the user and then offers a menu with the four basic operations:
addition, subtraction, multiplication, and division. Once the user has selected an operation from the menu, the
calculator performs the operation.
Hint: In the same way that there exists an Integer.parseInt() method to parse integers, there is a Dou-
ble.parseDouble() method to parse real numbers.
*/

System.out.print("Please enter a number: ");
String str = scanner.next();
double x =Double.parseDouble(str);

System.out.print("Please enter another number: ");
str = scanner.next();
double y =Double.parseDouble(str);

System.out.println("Please enter an operation");
System.out.println("1 for +");
System.out.println("2 for -");
System.out.println("3 for /");
System.out.println("4 for *");
str = scanner.next();
int mathOperator = Integer.parseInt(str);


if(mathOperator == 1){
	double mathResult = x+y;
	System.out.println("The result is " + mathResult);
} else if(mathOperator == 2){
	double mathResult = x-y;
	System.out.println("The result is " + mathResult);
} else if (mathOperator == 3 ){
	double mathResult = x/y;
	System.out.println("The result is " + mathResult);
}else if (mathOperator == 4){
	double mathResult = x*y;
	System.out.println("The result is " + mathResult);
} else{
	System.out.println("Not a valid mathematical operator");
}
	
}}

