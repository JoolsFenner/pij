import java.io.File;

public class Mkdir{
	public static void main(String[] args){
		String newDir = args[0];
		new Mkdir().makeDir(newDir);
	}
	private void makeDir(String newDir){
		File directory = new File (newDir);
		if(!directory.exists()){
			System.out.println("creating directory: " + directory);
			directory.mkdir(); 
		}
	}
}