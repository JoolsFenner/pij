import java.io.*;

/* 
ENTER THIS IN COMMAND LINE
*/

public class AvgTemp{
	private void launch(String[] fileNamesArray){
		File sourceFile = new File(fileNamesArray[0]);
		if(!sourceFile.exists()){
			throw new IllegalArgumentException(sourceFile + " doesn't exist");
		}
	
		calculateAverage(sourceFile);
	}
	
	public void calculateAverage(File sourceFile){
		BufferedReader in = null;
		String line;
		String[] lineSplit;
		
		int lineAvg;
		int numberCount = 0;
		int fileAvg = 0;
		
		
		try{
			in = new BufferedReader(new FileReader(sourceFile));

			while((line = in.readLine()) != null){
				lineSplit = line.split(",");
				lineAvg = 0;	
					
				for(int i = 0; i < lineSplit.length; i++){
					lineAvg += Integer.parseInt(lineSplit[i]);
					numberCount ++;
				}
				
				fileAvg += lineAvg;
				
				System.out.println("Line Average is " + (lineAvg / lineSplit.length));
			}
			
			System.out.println("");
			System.out.println("Average for file is " + (fileAvg/numberCount));
			
		} catch (FileNotFoundException ex){
			ex.printStackTrace();
		} catch (IOException ex){
			ex.printStackTrace();
		} finally{
			closeReader(in);
		}
	}
	
	private void closeReader (Reader reader){
		try{
			if(reader != null){
				reader.close();
			}
		} catch(IOException ex){
			ex.printStackTrace();
		}
	}
	
	
	public static void main(String[] args){
		new AvgTemp().launch(args);
	}
	
	
}