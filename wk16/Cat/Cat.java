import java.io.*;

public class Cat{
	public static void main(String[] args){
		String[] fileNameArray = new String[args.length];
		
		for(int i = 0; i < args.length; i++){
			fileNameArray[i] = args[i];
		}
		
		Cat c = new Cat();
		c.launch(fileNameArray);
	}
	
	private void launch(String[] fileNameArray){
		for(String next : fileNameArray){
			readFile(next);
		}
	}
	
	public void readFile(String fileName){
		if(fileExists(fileName)){
			readFileContents(fileName);
		} else{
			System.out.println(fileName + " does not exist" );
		}
	}
	
	public boolean fileExists(String filename){
		File file = new File(filename);
		return file.exists();
	}
	
	public void readFileContents(String filename){
		File file = new File(filename);
		BufferedReader in = null;
		try{	
			in = new BufferedReader(new FileReader(file));
			String line;
			while ((line = in.readLine()) != null) {
				System.out.println(line);
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex){
			ex.printStackTrace();
		} finally{
			closeReader(in);
		}	
	}
	
	private void closeReader(Reader reader){
		try{
		if(reader != null){
			reader.close();
		}
		} catch(IOException ex){
			ex.printStackTrace();
		}
	}

}