import java.io.*;

/* 
ENTER THIS IN COMMAND LINE
*/

public class Tr{
	private void launch(String[] fileNamesArray){
		File sourceFile = new File(fileNamesArray[0]);
		if(!sourceFile.exists()){
			throw new IllegalArgumentException(sourceFile + " doesn't exist");
		}
		
		String sourceStr = fileNamesArray[1];
		String replaceStr = fileNamesArray[2];
		
		readReplace(sourceFile, sourceStr, replaceStr);
	
	}
	
	public void readReplace(File sourceFile, String sourceStr, String replaceStr){
		BufferedReader in = null;
		String line;
		String[] lineSplit;
		
		try{
			in = new BufferedReader(new FileReader(sourceFile));

			while((line = in.readLine()) != null){
				lineSplit = line.split(" ");
				
				for(String next : lineSplit){
					if (next.equals(sourceStr)){
						System.out.print(replaceStr + " ");
					} else{
						System.out.print(next + " ");
					}
				}
				
				System.out.println();
				
			}
			
		} catch (FileNotFoundException ex){
			ex.printStackTrace();
		} catch (IOException ex){
			ex.printStackTrace();
		} finally{
			closeReader(in);
		}
	}
	
	private void closeReader (Reader reader){
		try{
			if(reader != null){
				reader.close();
			}
		} catch(IOException ex){
			ex.printStackTrace();
		}
	}
	
	
	public static void main(String[] args){
		new Tr().launch(args);
	}
	
	
}