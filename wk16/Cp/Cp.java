import java.io.*;
/* 
ENTER THIS IN COMMAND LINE  java Cp TestData.txt TestData2.txt TestData3.txt TestData4.txt TestData5.txt data
*/
public class Cp{
	public static void main(String[] args){
		new Cp().launch(args);
	}
	
	private void launch(String[] fileNamesArray){
		File sourceFile = new File(fileNamesArray[0]);
		File destinationFile;
		File destinationDirectory = new File (fileNamesArray[fileNamesArray.length-1]);
		
		if(!sourceFile.exists()){
			throw new IllegalArgumentException(sourceFile + " doesn't exist");
		}
		
		if(!destinationDirectory.exists()){
			throw new IllegalArgumentException(destinationDirectory + " doesn't exist");
		}
		
		for(int i = 1; i < fileNamesArray.length - 1; i++){
			destinationFile = new File(fileNamesArray[i]);
			copySrcDst(sourceFile, destinationFile, destinationDirectory);
		}	
	}
		
	public void copySrcDst(File sourceFile, File destinationFile, File destinationDirectory){
		BufferedReader in = null;
		PrintWriter out = null;
		try{	
			in = new BufferedReader(new FileReader(sourceFile));
			out = new PrintWriter(destinationDirectory + "/" + destinationFile);
			String line;
			
			while ((line = in.readLine()) != null) {
				out.write(line);
			}
			
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex){
			ex.printStackTrace();
		} finally{
			closeReader(in);
			out.close();
		}	
	}
	
	private void closeReader(Reader reader){
		try{
			if(reader != null){
				reader.close();
			}
		} catch(IOException ex){
			ex.printStackTrace();
		}
	}
}