public class TeacherLauncher{
	public static void main(String[] args){
		TeacherLauncher tl = new TeacherLauncher();
		tl.launch();
	}
	
	private void launch(){
		Lecturer julian = new Lecturer("Julian Fenner");
		julian.getName();
		julian.doResearch("Java programming");
	}
}