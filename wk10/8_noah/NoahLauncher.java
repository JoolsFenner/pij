public class NoahLauncher{
	public static void main(String[] args){
		NoahLauncher nl = new NoahLauncher();
		nl.launch();
	}
	private void launch(){
		System.out.println();
		
		Animal pigeon = new AirAnimal("Pigeon");
		pigeon.call();
		pigeon.reproduce();
		System.out.println();
		
		
		Animal salmon = new WaterAnimal("Salmon");
		salmon.call();
		salmon.reproduce();
		System.out.println();
		
		Animal lion = new MammalLandAnimal("Lion");
		lion.call();
		lion.reproduce();
		System.out.println();
		
		Animal whale = new MammalWaterAnimal("Whale");
		whale.call();
		whale.makeSound("hello I am a whale");
		whale.reproduce();
		System.out.println();
		
		
	}
}