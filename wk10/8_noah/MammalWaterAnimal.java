public class MammalWaterAnimal extends WaterAnimal{
	public MammalWaterAnimal(String name){
		super(name);
	}
	
	/**
	* @override
	*
	*/
	public void reproduce(){
		giveBirth();
	};
	private void giveBirth(){
		System.out.println(this.getName() + " giving birth");	
	}
	
}