public class WaterAnimal extends Animal{
	public WaterAnimal(String name){
		super(name);
	}
	
	/**
	* @override
	*
	*/
	public void call(){
		System.out.println(this.getName() + " not coming, I live in the sea");
	}
}