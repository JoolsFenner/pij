public abstract class Animal{
	private String animalName;
	public Animal(String animalName){
		this.animalName = animalName;
	}
	
	public void call(){
		System.out.println(this.getName() + " coming now");
	}
	
	/**LayEgg is implemented in abstract class because air, land and water animals all this whereas only the *mammal 'class' of land animals tend to give birth with exception of whales.  This therefore landanimal *class needs to have 1 subclass MammalLandAnimal and WaterAnimal needs 1 subclass MammalWaterAnimal
	*/
	public void reproduce(){
		layEgg();
	};
	private void layEgg(){
		System.out.println(this.getName() + " laying Egg");	
	}
	
	public void makeSound(String str){
		System.out.println(str);
	};
	public String getName(){
		return animalName;
	}
}