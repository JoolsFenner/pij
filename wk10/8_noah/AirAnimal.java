public class AirAnimal extends Animal{
	public AirAnimal(String name){
		super(name);
	}
	
	/**
	* @override
	*
	*/
	public void call(){
		System.out.println(this.getName() + " coming later if tired");
	}
	
}