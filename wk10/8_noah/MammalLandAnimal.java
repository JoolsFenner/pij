public class MammalLandAnimal extends LandAnimal{
	public MammalLandAnimal(String name){
		super(name);
	}
	
	/**
	* @override
	*
	*/
	public void reproduce(){
		giveBirth();
	};
	private void giveBirth(){
		System.out.println(this.getName() + " giving birth");	
	}
	
}