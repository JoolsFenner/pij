public class OldPhone implements Phone{
	private String brand = null;
	public OldPhone(String brand) {
		
		this.brand = brand;
	}
	public String getBrand() {
		System.out.println("Brand = " + brand);
		return brand;
	}
	// ... there is no setter for brand
	
	//methods
	public void call(String number){
		System.out.println("Calling " + number);
	}
}