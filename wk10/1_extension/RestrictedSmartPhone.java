public class RestrictedSmartPhone extends SmartPhone {
	public RestrictedSmartPhone(String brand){
		super(brand);
	}
	
	/**
	*@Override
	*
	*/
	public void playGame(){
		System.out.println("Restricted phone, game not loaded");
	}
}