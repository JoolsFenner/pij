public class SmartPhone extends MobilePhone {
	
	public SmartPhone(String brand){
		super(brand);
	}
	
	public void browseWeb(){
		System.out.println("Browsing web");
	}
	
	public void findPosition(){
		System.out.println("Your GPS coordinates are xx/yy");
	}
	
	
	/**
	*@Override
	*
	*/
	public void call(String number){
		if(number.charAt(0)=='0'&& number.charAt(1)=='0'){
			System.out.println("Calling " + number + " through the internet to save money");
		} else{
			super.call(number);
		}
	}
	
}