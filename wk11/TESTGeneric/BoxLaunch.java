public class BoxLaunch{
	public static void main(String[] args){
		new BoxLaunch().launch();
	}
	
	private void launch(){
		Box<String> jStr = new Box<String>("Jools"); 
		System.out.println(jStr.get());
		
		Box<Integer> jInt = new Box<Integer>(11); 
		System.out.println(jInt.get());
		
		Box<Double> jDouble= new Box<Double>(11.0); 
		System.out.println(jDouble.get());
		
	
		
	}
}