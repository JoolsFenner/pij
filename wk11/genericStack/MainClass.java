import java.util.EmptyStackException;

public class MainClass {

    public static void main(String args[]) {
        double[] doubleElements = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6};
        int[] integerElements = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

        Stack<Double> doubleStack = new StackImpl<>();
        Stack<Integer> integerStack = new StackImpl<>();
        //Stack<String> stringStack = new StackImpl<>();


        // test Push Double
        System.out.println("\nPushing elements onto doubleStack");

        for (double element : doubleElements) {
            System.out.printf("%.1f ", element);
            doubleStack.push(element);
        }

        // test Pop Double

        System.out.println("\nPopping elements from doubleStack");
        double popValue;

        try {
            while (true) {
                popValue = doubleStack.pop(); // pop from doubleStack
                System.out.printf("%.1f ", popValue);
            }
        } catch (EmptyStackException ex) {
        }

        // test push method with integer stack
        System.out.println("\nPushing elements onto integerStack");

        for (int element : integerElements) {
            System.out.printf("%d ", element);
            integerStack.push(element);
        }

        // test pop method with integer stack

        System.out.println("\nPopping elements from integerStack");
        int popvalue; // store element removed from stack

        // remove all elements from Stack
        try {
            while (true) {
                popvalue = integerStack.pop();
                System.out.printf("%d ", popvalue);
            }
        } catch (EmptyStackException ex) {

        }

    }
}