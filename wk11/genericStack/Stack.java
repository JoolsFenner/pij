public interface Stack<E extends Number> {
    void push(E pushValue);

    E pop();

    boolean empty();
}
