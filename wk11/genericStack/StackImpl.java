import java.util.ArrayList;
import java.util.EmptyStackException;

class StackImpl<E extends Number> implements Stack<E> {
    private ArrayList<E> elements;

    public StackImpl() {
        elements = new ArrayList<>();
    }

    @Override
    public void push(E pushValue) {
        elements.add(pushValue); // place pushValue on Stack
    }

    @Override
    public E pop() {
        if (empty()) // if stack is empty
            throw new EmptyStackException();

        return elements.remove(elements.size()-1); // remove and return top element of Stack
    }

    @Override
    public boolean empty(){
        return elements.size() == 0;
    }
}