public class Comparator {
	
	public double getMax(double d1, double d2) {
	//comparision done here
		if (d1 > d2) {
			return d1;
		} else {
			return d2;
		}
	}
	
	
	public int getMax(int n, int m) {
	//converts int to double
		double d1 = (double) n;
		double d2 = (double) m;                
		int result = (int) getMax(d1, d2); 
		return result; 
	}
		
	public String getMax(String number1, String number2) {
	//converts string to double
		double d1 = Double.parseDouble(number1);
		double d2 = Double.parseDouble(number2);
		return String.valueOf(getMax(d1, d2));			
	}
	
	public static void main(String[] args){
		Comparator c = new Comparator();
		c.launch();
	}
	
	private void launch(){
		System.out.println(getMax("1","2"));
	}
	
}