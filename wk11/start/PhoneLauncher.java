public class PhoneLauncher{
	public static void main(String[] args){
		new PhoneLauncher().launch() ;
		
	}
	private void launch(){
		MobilePhone jMobileSmartPhone = new SmartPhone("Nokia");
		
		//direct upcasting
		//((SmartPhone)jMobileSmartPhone).browseWeb();
		
		testPhone(jMobileSmartPhone);
	}

	private void testPhone(Phone phone){
		phone.call("12345");
		
		//downcasting
		SmartPhone jSmartPhone = (SmartPhone) phone;
		jSmartPhone.browseWeb();
	}
	
}