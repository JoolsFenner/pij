public class NationalInsurance {
	private String name;
	private int natIntNumber;
	public NationalInsurance(String name, int number) {
		this.name = name;
		this.natIntNumber = number;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getNatInt(){
		return this.natIntNumber;
	}
		
}