public class Company {

	public static void main(String[] args) {	
		new Company().launch();
	}
	
	public void launch(){
		CompanyList employeeList = new CompanyList<Employee>();
		Employee emp1 = new Employee("Julian");
		employeeList.add(emp1);
		
		String jString = "junk";
		employeeList.add(jString);
		
		
		/*
		
		CompanyList employeeList = new CompanyList();
		employeeList.add(new Employee("employee1"));
		employeeList.add(new Employee("employee2"));
		employeeList.add(new Employee("employee3"));
		employeeList.add(new Employee("employee4"));
		employeeList.add(new Employee("employee5"));
		
		employeeList.printList();
		System.out.println();
		
		CompanyList nInsList = new CompanyList();
		nInsList.add(new NationalInsurance("Julian", 12342));
		nInsList.add(new NationalInsurance("David", 54321));
		nInsList.add(new NationalInsurance("Andrea", 98765));
	
		//THIS WONT WORK UNTIL PRINT LIST METHOD IS MADE GENERIC
		//	nInsList.printList();
		
		
		*/
		
	}
	
}