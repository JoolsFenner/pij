public class Company {

	public static void main(String[] args) {	
		new Company().launch();
	}
	
	public void launch(){
		CompanyList<String> employeeList = new CompanyList<String>();
		employeeList.add(new String("Julian"));
		employeeList.add(new String("David"));
		employeeList.add(new String("Graciela"));
		employeeList.add(new String("Andrea	"));
		
		employeeList.printList();
		
		System.out.println();
		System.out.println();
		System.out.println("Printing National Insurance Numbers");
		System.out.println();
		CompanyList<Integer> nationalInsuranceList = new CompanyList<Integer>();
		nationalInsuranceList.add(new Integer(111111));
		nationalInsuranceList.add(new Integer(222222));
		nationalInsuranceList.add(new Integer(333333));
		nationalInsuranceList.add(new Integer(444444));
		
		nationalInsuranceList.printList();
	}
	
	
	
}