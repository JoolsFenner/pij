public class Company {

	public static void main(String[] args) {	
		new Company().launch();
	}
	
	public void launch(){
		CompanyList<Employee> employeeList = new CompanyList<Employee>();
		
		
		employeeList.add(new Employee("employee1"));
		employeeList.add(new Employee("employee2"));
		employeeList.add(new Employee("employee3"));
		employeeList.add(new Employee("employee4"));
		employeeList.add(new Employee("employee5"));
		
		System.out.println("PRINT EMPLOYEE LIST");
		
		//System.out.println(employeeList.getHead().getListObject().getClass());
		
		employeeList.printList();
		System.out.println();
		
		/*System.out.println("PRINT NATIONAL INSURANCE LIST");
		
		CompanyList nInsList = new CompanyList();
		nInsList.add(new NationalInsurance("Julian", 12342));
		nInsList.add(new NationalInsurance("David", 54321));
		nInsList.add(new NationalInsurance("Andrea", 98765));
	
		//THIS WONT WORK UNTIL PRINT LIST METHOD IS MADE GENERIC
			nInsList.printList();
		/**/
	}
	
}