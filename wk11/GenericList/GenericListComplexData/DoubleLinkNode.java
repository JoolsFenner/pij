public class DoubleLinkNode<T>{
	private T t;
	private DoubleLinkNode next;
	private DoubleLinkNode previous;
	public DoubleLinkNode(T t){
		this.t = t;
		this.next = null;
		this.previous = null;
	}
		
	public void setNext(DoubleLinkNode newNode){
		if(this.next==null){
			this.next = newNode;
			newNode.previous = this;
			return;
		}
		this.next.setNext(newNode);
		
	}
	public T getListObject(){
		return t;
	}
	
	public DoubleLinkNode getNext(){
		return this.next;
	}
	public DoubleLinkNode getPrevious(){
		return this.previous;
	}
}