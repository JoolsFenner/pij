public class CompanyList<T>{
	private DoubleLinkNode head;
	
	public void add(T t){
		DoubleLinkNode<T> newNode = new DoubleLinkNode<T>(t);
		if(head == null){
		//if head is null make head and end point to first node.
			head = newNode;
			return;
		}
		head.setNext(newNode);	
	}
	
	public void add(NationalInsurance ni){
		DoubleLinkNode<NationalInsurance> newNode = new DoubleLinkNode<NationalInsurance>(ni);
		if(head == null){
		//if head is null make head and end point to first node.
			head = newNode;
			return;
		}
	
		head.setNext(newNode);
		
	}
	
	public void printList(){
		printList(head);
	}
	private void printList(DoubleLinkNode node){	
		DoubleLinkNode aux = node;
		boolean endLoop = false;
			do{			
				System.out.println(aux);		
				//System.out.println(((Employee)aux.getListObject()).getName());
				System.out.println(aux.getListObject());
			
				System.out.println("next = " + aux.getNext());
				System.out.println("previous = " + aux.getPrevious());
				
				System.out.println();
				
				if(aux.getNext()!=null){
					aux = aux.getNext();
				} else {
					endLoop = true;
				}
				
			}while(!endLoop);	
	}
	
	public DoubleLinkNode getHead(){
		return head;
	}
	
}