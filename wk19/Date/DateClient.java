import java.rmi.NotBoundException;
import java.rmi.Naming;
import java.rmi.Remote;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Calendar;	

//java -Djava.security.policy=client.policy Client <someTextHere>
public class DateClient {


	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException{
	
		Remote service = Naming.lookup("//127.0.0.1:1099/date");
		DateService dateService = (DateService) service;
		System.out.println("Press key for date 0 to exit");
		String str = System.console().readLine();
		
		while(!str.equals("0")){
			Calendar receivedDate = dateService.returnDate();
			System.out.println(receivedDate);
			System.out.println("Press key for date 0 to exit");
			str = System.console().readLine();
			
		};
	}
}