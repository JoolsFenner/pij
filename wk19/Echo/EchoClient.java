import java.rmi.NotBoundException;
import java.rmi.Naming;
import java.rmi.Remote;
import java.net.MalformedURLException;
import java.rmi.RemoteException;

//java -Djava.security.policy=client.policy Client <someTextHere>
public class EchoClient {


	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException{
	
		Remote service = Naming.lookup("//127.0.0.1:1099/echo");
		EchoService echoService = (EchoService) service;
		System.out.println("Insert word to send 0 to exit");
		String str = System.console().readLine();
		
		while(!str.equals("0")){
			String receivedEcho = echoService.echo(str);
			System.out.println("Insert next word to send, 0 to exit");
			str = System.console().readLine();
			
		};
	}
}