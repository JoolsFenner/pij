import java.util.List;
import java.util.ArrayList;

public class ArrayListAddSet{
	
	private void launch(){
		ArrayList list = new ArrayList();
			list.add("P");
			list.add("Q");
			list.add("R");
			list.set(2,"s"); //from List<E>. Changes element 2 (0 indexed)
			list.add(2,"T"); // adds at position 2 i.e between U and L in this case  (0) J (1) U (2) L (3) I (4) A (5) N ( 6)
			list.add("u");
			System.out.println(list);
	}
	
	public static void main(String[] args){
		new ArrayListTest().launch();
	}
}