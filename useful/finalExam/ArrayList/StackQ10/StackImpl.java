import java.util.ArrayList;

public <E>class StackImpl<E> implements Stack<E>{

		private ArrayList<E> stack;

		public StackImpl(){
			this.stack = new ArrayList<E>();
		}
		
		public boolean isEmpty(){
			if(stack.size()==0){
				return true;
			}else{
				return false;
			}
		}
		
		public E peek(){
			return stack.get(stack.size()-1);
		}
		
		public E pop(){
			return stack.remove(stack.size()-1);
		}
		
		public void push(E item){
			stack.add(item);
		}
}