public class Launcher{
	
	private void launch(){
		Animal jools = new Person();
		// jools.talk();  <-- won't compile 
		
		Animal steve = (Person) new Person();
		//steve.talk();  <-- won't compile
		
		((Person)steve).talk(); //<--correct, not great to do this normally though.
	}
	public static void main(String[] args){
		new Launcher().launch();
	}
	
	public void talk(){
		System.out.println("talk");
	}
}