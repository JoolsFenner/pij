public class Base{
	int number;
	String word;
	
	public Base(int number, String word){
		this.number = number;
		this.word = word;
	}
	
	public int getNumber(){
		System.out.println("number is being returned from base");
		return number;
	}
	
	@Override
	public String toString(){
		return "Number = " + this.number + " word = " + this.word;
	}
}