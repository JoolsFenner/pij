public class Launcher{

	private void launch(){
		//look at difference when switching the two lines below
		
		Derived d = new Derived(1, "derived word");
		//Derived d = new Derived();
		d.getNumber();
	}

	public static void main(String[] args){
		new Launcher().launch();
	}
}