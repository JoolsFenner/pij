public class Derived extends Base{
	
	
	public Derived(){
		super(9, "base word");
	}
	
	public Derived(int number, String word){
		this();	
		this.number = number;
		this.word = word;
	}
	
	@Override
	public int getNumber(){
		System.out.println("number is being returned from derived");
		System.out.println(this); //calls toString
		return number;
	}
	
}