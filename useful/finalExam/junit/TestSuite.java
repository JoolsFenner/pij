import org.junit.*;
import static org.junit.Assert.*;
import org.junit.rules.ExpectedException;


public class TestContactManager {
	
	//variables & fields for use in tests
	ContactManager testContactManager;
	MakeId idGenerator;
	int testContact1Id;
	int testContact2Id;
	File file;
	
	@BeforeClass{
		//run once per execution at the start of test cycle
	}
	@AfterClass{
		//run once per execution at the end of test cycle
	}
	
	@Before
	public void buildUpMain() {
		testContactManager = new ContactManagerImpl();
		idGenerator = new MakeIdImpl();
		
		testContactManager.addNewContact("Test Contact 1", "Java programmer");
		testContact1Id= idGenerator.getId();
		testContactManager.addNewContact("Test Contact 2", "Groovy programmer");
		testContact2Id = idGenerator.getId();
				
		meeting1Participants =  testContactManager.getContacts(testContact1Id, testContact2Id, testContact3Id);
		meeting2Participants =  testContactManager.getContacts(testContact4Id, testContact2Id);
		meeting3Participants =  testContactManager.getContacts(testContact3Id);
		meeting4Participants =  testContactManager.getContacts(testContact2Id, testContact3Id, testContact4Id);
		
		file = new File("Contacts.txt");
	}
	
	@After
	public void tearDown(){
		if (file.exists()) file.delete();
	}

	//assertsEquals test if two state are equal
	@Test
	public void addFutureMeetingTest_testIdReturn(){
		int expectedId = idGenerator.getId()+1;
		int outputId = testContactManager.addFutureMeeting(meeting1Participants, futureMeetingDate);
		assertEquals(java.lang.Object object);
	}
	
	assertTrue(boolean condition);
	assertFalse(boolean condition);
	assertNull(java.lang.String message,java.lang.Object object)
	
	
	
	//@Test(expected) = Exception.class
	
	@Test(expected = IllegalArgumentException.class)
	public void addFutureMeeting_withPastDate(){
	//test
	}
	
	@Test(expected = IllegalArgumentException.class)
	
	@Test(expected = NullPointerException.class)
	
	
	