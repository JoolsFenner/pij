import org.junit.*;
import static org.junit.Assert.*;
import org.junit.rules.ExpectedException;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


public class TestContactManager {
	ContactManager testContactManager;
	MakeId idGenerator;
	
	//Contact Data.
	int testContact1Id;
	int testContact2Id;
	int testContact3Id;
	int testContact4Id;
	
	//meeting participants data
	Set<Contact> meeting1Participants;
	Set<Contact> meeting2Participants;
	Set<Contact> meeting3Participants;
	Set<Contact> meeting4Participants;
	
	//to test unkwown participants
	private Contact unknownParticipant;
	Set<Contact> unknownMeetingParticipants;
	
	//dates for testing
	Calendar futureMeetingDate;
	Calendar pastMeetingDate;
	
	//dates for testing future date order in methods like getFutureMeetingList.  
	Calendar tempFutureMeetingDate2018;
	Calendar tempFutureMeetingDate2019;
	Calendar tempFutureMeetingDate2020;
	Calendar tempFutureMeetingDate2021;
	Calendar tempFutureMeetingDate2022;
	//for checking order of return value
	Calendar[] tempFutureMeetingOrderExpected = new Calendar[5];
	
	File file;
	
	@Before
	public void buildUpMain() {
		idGenerator = new MakeIdImpl();
		
		testContactManager = new ContactManagerImpl();
		
		testContactManager.addNewContact("Test Contact 1", "Java programmer");
		testContact1Id= idGenerator.getId();
		testContactManager.addNewContact("Test Contact 2", "Silicon Valley vc");
		testContact2Id = idGenerator.getId();
		testContactManager.addNewContact("Test Contact 3", "C# Developer");		
		testContact3Id = idGenerator.getId();
		testContactManager.addNewContact("Test Contact 4", "Javascript Developer");
		testContact4Id = idGenerator.getId();
		
		meeting1Participants = new HashSet<Contact>();
		meeting2Participants = new HashSet<Contact>();
		meeting3Participants = new HashSet<Contact>();
		meeting4Participants = new HashSet<Contact>();
		
		
		meeting1Participants =  testContactManager.getContacts(testContact1Id, testContact2Id, testContact3Id);
		meeting2Participants =  testContactManager.getContacts(testContact4Id, testContact2Id);
		meeting3Participants =  testContactManager.getContacts(testContact3Id);
		meeting4Participants =  testContactManager.getContacts(testContact2Id, testContact3Id, testContact4Id);
		
		unknownParticipant = new ContactImpl("Unknown Person", "notes", 999);
		unknownMeetingParticipants = new HashSet<Contact>();
		unknownMeetingParticipants.add(unknownParticipant);
				
		futureMeetingDate = new GregorianCalendar(2016,7,1);
		pastMeetingDate = new GregorianCalendar(2013,9,14);		
		
		tempFutureMeetingDate2018 = new GregorianCalendar(2018,7,1);
		tempFutureMeetingOrderExpected[0] = tempFutureMeetingDate2018;
		tempFutureMeetingDate2019 = new GregorianCalendar(2019,7,1);
		tempFutureMeetingOrderExpected[1] = tempFutureMeetingDate2019;
		tempFutureMeetingDate2020 = new GregorianCalendar(2020,7,1);
		tempFutureMeetingOrderExpected[2] = tempFutureMeetingDate2020;
		tempFutureMeetingDate2021 = new GregorianCalendar(2021,7,1);
		tempFutureMeetingOrderExpected[3] = tempFutureMeetingDate2021;
		tempFutureMeetingDate2022 = new GregorianCalendar(2022,7,1);
		tempFutureMeetingOrderExpected[4] = tempFutureMeetingDate2022;
		
		//meeting test data
		testContactManager.addFutureMeeting(meeting1Participants, futureMeetingDate);
		testContactManager.addFutureMeeting(meeting1Participants, futureMeetingDate);
		testContactManager.addFutureMeeting(meeting3Participants, futureMeetingDate);
		testContactManager.addFutureMeeting(meeting2Participants, futureMeetingDate);
		testContactManager.addFutureMeeting(meeting4Participants, futureMeetingDate);
		testContactManager.addNewPastMeeting(meeting1Participants, pastMeetingDate, "meeting notes go here");
		testContactManager.addNewPastMeeting(meeting4Participants, pastMeetingDate, "meeting notes go here");
 
		file = new File("Contacts.txt");
	}
	
	
	@After
	public void tearDown(){
		/* reinitialises static int used for allocating id.  This method is for reInitialising last used id from data file but is being used here to reset unit testing environment */
		idGenerator.reInitialiseId(0); 
	
		if (file.exists()) file.delete();
	}

	
	/**
	 * addFutureMeeting(Set<Contact> contacts, Calendar date) tests
	 *
	*/
	@Test
	public void addFutureMeetingTest_testIdReturn(){
		int expectedId = idGenerator.getId()+1;
		int outputId = testContactManager.addFutureMeeting(meeting1Participants, futureMeetingDate);
		assertEquals(expectedId, outputId);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addFutureMeeting_withPastDate(){
		Calendar meetingDatePast = new GregorianCalendar(2013,7,1);
		testContactManager.addFutureMeeting(meeting1Participants, meetingDatePast);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addFutureMeeting_withUnknownContact(){
		testContactManager.addFutureMeeting(unknownMeetingParticipants, futureMeetingDate);
	}
	
	
	
	/**
	 * PastMeeting getPastMeeting(int id) tests
	 *
	*/
	@Test
	public void getPastMeeting_returnTest(){
		testContactManager.addNewPastMeeting(meeting1Participants, pastMeetingDate, "meeting went well");
		int lastAllocatedMeetingId = idGenerator.getId();
		Meeting pastMeeting = testContactManager.getPastMeeting(lastAllocatedMeetingId);
		int returnedPastMeetingId = pastMeeting.getId();
		assertEquals(lastAllocatedMeetingId,returnedPastMeetingId);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void getPastMeeting_futureDate(){
		int lastAllocatedMeetingId = testContactManager.addFutureMeeting(meeting1Participants, futureMeetingDate);
		testContactManager.getPastMeeting(lastAllocatedMeetingId);
	}
	
	@Test 
	public void getPastMeeting_meetingDoesNotExist(){
		Meeting output = testContactManager.getPastMeeting(999);
		assertEquals(null,output);
	}
	
	
	
	/**
	 * FutureMeeting getFutureMeeting(int id) tests
	 *
	*/
	@Test
	public void getFutureMeeting_returnTest(){
		int lastAllocatedMeetingId = testContactManager.addFutureMeeting(meeting1Participants, futureMeetingDate);
		Meeting futureMeeting = testContactManager.getFutureMeeting(lastAllocatedMeetingId);
		int returnedFutureMeetingId = futureMeeting.getId();
		assertEquals(lastAllocatedMeetingId,returnedFutureMeetingId);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void getFutureMeeting_pastDate(){
		testContactManager.addNewPastMeeting(meeting1Participants, pastMeetingDate, "meeting went well");
		int lastAllocatedMeetingId = idGenerator.getId();
		testContactManager.getFutureMeeting(lastAllocatedMeetingId);
	}
	
	@Test 
	public void getFutureMeeting_meetingDoesNotExist(){
		Meeting output = testContactManager.getFutureMeeting(999);
		assertEquals(null,output);
	}
	
	
	
	/**
	 * Meeting getMeeting(int id) tests
	 *
	*/
	@Test
	public void getMeeting_returnTestFutureMeeting(){
		int lastAllocatedMeetingId = testContactManager.addFutureMeeting(meeting1Participants, futureMeetingDate);
		Meeting meeting = testContactManager.getMeeting(lastAllocatedMeetingId);
		int returnedMeetingId = meeting.getId();
		assertEquals(lastAllocatedMeetingId,returnedMeetingId);
	}
	
	@Test
	public void getMeeting_returnTestPastMeeting(){
		testContactManager.addNewPastMeeting(meeting1Participants, pastMeetingDate, "meeting went well");
		int lastAllocatedMeetingId = idGenerator.getId();
		Meeting meeting = testContactManager.getMeeting(lastAllocatedMeetingId);
		int expectedId = meeting.getId();
		assertEquals(lastAllocatedMeetingId,expectedId);
	}
	
	@Test 
	public void getMeeting_meetingDoesNotExist(){
		Meeting output = testContactManager.getMeeting(999);
		assertEquals(null,output);
	}
	
	
	/**
	 * List<Meeting> getFutureMeetingList(Contact contact) tests
	 *
	*/
	@Test 
	public void getFutureMeetingListByContact_returnTest(){
		Contact testContact1 = null;
		for(Contact next : meeting1Participants){
			if(next.getId()== testContact1Id){
				testContact1 = next;
			}		
		}	
		
		List<Meeting> returnedList = testContactManager.getFutureMeetingList(testContact1);
		int expectedListSize = 2; // because testcontact1 is in 2 future meeting (see build up data)
		int outputListSize = returnedList.size();		
		assertEquals(expectedListSize,outputListSize);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void getFutureMeetingListByContact_contactNotInList(){
			testContactManager.getFutureMeetingList(unknownParticipant);
	}
	
	@Test
	public void getFutureMeetingListByContact_testSortOrder(){

		//get contact for testing
		testContactManager.addNewContact("Temporary Contact", "Hardware engineer");
		Set<Contact> tempSet = testContactManager.getContacts("Temporary Contact");
		Contact temporaryContact = new ContactImpl("",0);
		for(Contact next : tempSet){
			temporaryContact=next;
		}
		Set<Contact> tempMeetingParticipants = new HashSet<Contact>();
		tempMeetingParticipants.add(temporaryContact);
		
		
		testContactManager.addFutureMeeting(tempMeetingParticipants, tempFutureMeetingDate2020);
		testContactManager.addFutureMeeting(tempMeetingParticipants, tempFutureMeetingDate2022);
		testContactManager.addFutureMeeting(tempMeetingParticipants, tempFutureMeetingDate2019);
		testContactManager.addFutureMeeting(tempMeetingParticipants, tempFutureMeetingDate2018);
		testContactManager.addFutureMeeting(tempMeetingParticipants, tempFutureMeetingDate2021);
		List<Meeting> testContactsMeetings = testContactManager.getFutureMeetingList(temporaryContact);
		
		boolean expected = true;
		boolean output = true;
		//compare date order return by method with order of tempFutureMeetingOrderExpected[]; 
		for(int i = 0; i < tempFutureMeetingOrderExpected.length; i++){	
			if(tempFutureMeetingOrderExpected[i] != testContactsMeetings.get(i).getDate()){
				output = false;
			}
		}
		
		assertEquals(expected, output);	
	}
	
	
	
	/**
	 * List<Meeting> getFutureMeetingList(Calendar date) tests
	 *
	*/
	@Test
	public void getFutureMeetingListByDate_returnTest(){
		List<Meeting> returnedList = testContactManager.getFutureMeetingList(futureMeetingDate);
		int expectedListSize = 5; //based on build up data
		int outputListSize = returnedList.size();
		assertEquals(expectedListSize, outputListSize);	
	}
	
	@Test
	public void getFutureMeetingListByDate_noMeetingOnDate(){
		Calendar tempDate = new GregorianCalendar(2030,1,1);
		List<Meeting> returnedList = testContactManager.getFutureMeetingList(tempDate);
		int expectedListSize = 0;
		int outputListSize = returnedList.size();
		assertEquals(expectedListSize, outputListSize);	
	}
	


	/**
	 * List<PastMeeting> getPastMeetingList(Contact contact) tests
	 *
	*/
	@Test
	public void getPastMeetingListByContact_returnTest(){
		Set<Contact> contacts = testContactManager.getContacts("Test Contact 1");
		Contact testContact1 = new ContactImpl("","",0);
		for(Contact next : contacts){
			testContact1 = next;
		}
		
		List<PastMeeting> returnedList = testContactManager.getPastMeetingList(testContact1);
		int expectedListSize = 1;
		int outputListSize = returnedList.size();
		assertEquals(expectedListSize, outputListSize);
	}

	@Test (expected = IllegalArgumentException.class)
	public void getPastMeetingListByContact_noSuchContact(){
		testContactManager.getPastMeetingList(unknownParticipant);
	}
	
	@Test
	public void getPastMeetingListByContact_contactHasNoMeetings(){
		testContactManager.addNewContact("Test Contact 999", "C# programmer");
		Set<Contact> contacts = testContactManager.getContacts("Test Contact 999");
		Contact testContact1 = new ContactImpl("","",0);
		for(Contact next : contacts){
			testContact1 = next;
		}
		
		List<PastMeeting> returnedList = testContactManager.getPastMeetingList(testContact1);
		assertEquals(0, returnedList.size());
	}
	
	
	
	/**
	 * void addNewPastMeeting(Set<Contact> contacts, Calendar date, String text) test
	 *
	*/	
	@Test(expected = NullPointerException.class)
	public void addNewPastMeeting_MissingArgument(){
		testContactManager.addNewPastMeeting(null, pastMeetingDate, "meeting went well");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addNewPastMeeting_contactNotInMap(){
		testContactManager.addNewPastMeeting(unknownMeetingParticipants, pastMeetingDate, "meeting went well");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addNewPastMeeting_emptyContactSet(){
		Set<Contact> emptyContactSet = new HashSet<Contact>();
		testContactManager.addNewPastMeeting(emptyContactSet, pastMeetingDate, "meeting went well");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addNewPastMeeting_futureDate(){
		testContactManager.addNewPastMeeting(meeting1Participants, futureMeetingDate, "meeting went well");
	}
	
	/**
	 *
	 *void addMeetingNotes(int id, String text) test
	 *
	*/
	@Test
	public void addMeetingNotes_addingToPastMeeting(){
		String meetingNotes1 = "meeting went well";
		String meetingNotes2 = "more thoughts on meeting";
		
		testContactManager.addNewPastMeeting(meeting1Participants, pastMeetingDate, meetingNotes1);
		int meetingId = idGenerator.getId();
		testContactManager.addMeetingNotes(meetingId, meetingNotes2);
		
		String expectedString = meetingNotes1 + ". " + meetingNotes2;
		PastMeeting meeting = (PastMeeting)testContactManager.getMeeting(meetingId);
		String outputString = meeting.getNotes();
			
		assertEquals(expectedString,outputString);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addMeetingNotes_meetingDoesNotExist(){
		testContactManager.addMeetingNotes(999, "some test notes");
	}
	
	@Test(expected = IllegalStateException.class)
	public void addMeetingNotes_FutureMeeting(){
		testContactManager.addFutureMeeting(meeting1Participants, futureMeetingDate);	
		int meetingId = idGenerator.getId();
		testContactManager.addMeetingNotes(meetingId, "some notes");
	}
	
	@Test (expected = NullPointerException.class)
	public void addMeetingNotes_nullNotes(){
		testContactManager.addNewPastMeeting(meeting1Participants, pastMeetingDate, "notes");
		int meetingId = idGenerator.getId();
		testContactManager.addMeetingNotes(meetingId, null);
	}
	
	
	
	/**
	 * void addNewContact(String name, String notes) test
	 *
	*/
	@Test
	public void addNewContactTest(){
		int expectedId = idGenerator.getId() +1;
		testContactManager.addNewContact("Steve", "silicon valley vc");
		int outputId = idGenerator.getId();	
		assertEquals(expectedId,outputId);
	}	
	
	@Test (expected = NullPointerException.class)
	public void addNewContactTest_nullNotes(){
		testContactManager.addNewContact("Steve", null);
	}
	
	@Test (expected = NullPointerException.class)
	public void addNewContactTest_nullName(){
		testContactManager.addNewContact(null, "notes");
	}

	
	
	/**
	 * Set<Contact> getContacts(int... ids) test
	 *
	*/
	@Test 
	public void getContactsById_returnTest(){
		testContactManager.addNewContact("tempContact1", "Java programmer");
		int tempContact1Id = idGenerator.getId();
		testContactManager.addNewContact("tempContact2", "Scala programmer");
		int tempContact2Id = idGenerator.getId();
		testContactManager.addNewContact("tempContact3", "Javascript programmer");
		int tempContact3Id = idGenerator.getId();
		
		Set<Contact> contactSet = new HashSet<Contact>();
		contactSet = testContactManager.getContacts(tempContact1Id,tempContact2Id,tempContact3Id);
		int expectedSetSize = 3;
		int outputSetSize = contactSet.size();
		
		assertEquals(expectedSetSize,outputSetSize);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void getContactsById_noSuchContact(){
		testContactManager.addNewContact("tempContact1", "Java programmer");
		int tempContact1Id = idGenerator.getId();
		testContactManager.getContacts(999);
	}
	
	/**
	 * Set<Contact> getContacts(String name) test
	 *
	*/
	@Test 
	public void getContactsByString_returnTest(){
		testContactManager.addNewContact("Brian Jones", "Java programmer");
		testContactManager.addNewContact("Peter Jones", "Perl programmer");
		testContactManager.addNewContact("Sarah Smith", "Fortran programmer");
		testContactManager.addNewContact("David Jonez", "C# programmer");
		
		Set<Contact> contactSet = new HashSet<Contact>();
		contactSet = testContactManager.getContacts("Jones");
		
		int expectedSetSize = 2;
		int outputSetSize = contactSet.size();
			
		assertEquals(expectedSetSize,outputSetSize);
	}
	
	@Test (expected = NullPointerException.class)
	public void getContactsByString_null(){	
		String str = null;
		testContactManager.getContacts(str);
		
	}	
	
	/**
	 * void flush() tests
	 *
	*/
		
	@Test 
	public void flushTest_multipleWriteRequests(){
		//Set up for test
		String localFileName = "TestContacts.txt";
		File localFile = new File(localFileName);
		if(localFile.exists()) localFile.delete();
		
		ContactManager localContactManager = new ContactManagerImpl(localFileName);
		
		localContactManager.addNewContact("Test Contact 1", "Java programmer");
		int localContact1Id= idGenerator.getId();
		localContactManager.addNewContact("Test Contact 2", "Java programmer");
		int localContact2Id = idGenerator.getId();
		
		Set<Contact> localMeetingParticipants = new HashSet<Contact>();
		localMeetingParticipants =  localContactManager.getContacts(localContact1Id, localContact2Id);
		localContactManager.addFutureMeeting(localMeetingParticipants, futureMeetingDate);
		localContactManager.addNewPastMeeting(localMeetingParticipants, pastMeetingDate, "meeting notes go here");
		
		//test start
		localContactManager.flush();
		int expectedFileLength = 235; //based on build up data.  If this test fails, changed build up data may be a cause
		long ouputFileLength = localFile.length();
		assertEquals(expectedFileLength, ouputFileLength);
		
	}
	
}