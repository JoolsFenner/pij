import java.util.Set;
import java.util.HashSet;

//collection of child

public class Institution<E>{
	
	Set<E> childSet;
	
	public PreSchool(){
		this.childSet = new HashSet<E>();
	}
	
	public void enroll(E c){
		childSet.add(c);
	}
	
	public void withdraw(E c){
		childSet.remove(c);
	}

}