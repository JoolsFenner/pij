import java.util.Set;
import java.util.HashSet;

//collection of child

public class PreSchool{
	
	Set<Child> childSet;
	
	public PreSchool(){
		this.childSet = new HashSet<Child>();
	}
	
	public void enroll(Child c){
		childSet.add(c);
	}
	
	public void withdraw(Child c){
		childSet.remove(c);
	}

}