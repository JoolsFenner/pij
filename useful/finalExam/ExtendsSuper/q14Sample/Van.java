class Van extends Vehicle {
	private int maxLoad;

	public Van(int a, int b, String s, int c) {
		super(a,b,s);
		this.maxLoad = c;
	}


	@Override
	public String toString() {
		return super.toString() + ", maxLoad " + maxLoad + " kgs";
	}
}