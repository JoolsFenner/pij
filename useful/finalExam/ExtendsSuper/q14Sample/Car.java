class Car extends Vehicle {
	private int numberSeats;
	private String modelName;

	public Car(int a, int b, String s, int c, String t) {
		super(a,b,s);
		this.numberSeats = c;
		this.modelName = t;
	}

	public Car(Vehicle v, int a, String s) {
		super(v.numberDoors, v.numberWheels, v.colour);
		this.numberSeats = a;
		this.modelName = s;
	}

	@Override
	public String toString() {
		return super.toString() + "numberSeats " + numberSeats + ", modelName " + modelName;
	}
}