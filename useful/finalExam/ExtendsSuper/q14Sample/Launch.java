import java.util.List;
import java.util.ArrayList;

public class Launch{

	public static void main(String[] args){
		Car c1 = new Car(4, 4, "blue", 4, "Ford Pro");
		Car c2 = new Car(4, 4, "red", 4, "Fiat Mars");
		Van v1 = new Van(4, 4, "white", 500); 
		ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
		vehicles.add(c1);
		vehicles.add(c2);
		vehicles.add(v1);
		
		for(Vehicle next : vehicles){
			System.out.println(next);
		}
		
	}
}