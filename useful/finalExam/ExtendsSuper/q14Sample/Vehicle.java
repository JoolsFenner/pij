public class Vehicle {
	protected int numberDoors;
	protected int numberWheels;
	protected String colour;
	public Vehicle(int a, int b, String s) {
		this.numberDoors = a;
		this.numberWheels = b;
		this.colour = s;
	}
	@Override
	public String toString() {
		return "numberDoors " + numberDoors + ", numberWheels " + numberWheels + ", colour" + colour;
	}
}