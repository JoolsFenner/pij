class SubSubConstructor extends SubConstructor {
	private String s;
	
	public SubSubConstructor() {
		super("item");
		s = super.s;
		x++;
		System.out.println("[6] [s = " + this.s + "] " + this);
	}
	
	public String toString() {
		//my code
		System.out.println("SubSub's s is " + s);
		System.out.println("Sub's s is " + super.s);
		//my code end
		return "******* " + super.toString();
	}
	
}