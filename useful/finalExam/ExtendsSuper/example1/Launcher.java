public class Launcher{
	
	private void launch(){
		Base b = new Derived();
		b.methodOne(0);
	}
	
	public static void main(String[] args){
		new Launcher().launch();
	}
}