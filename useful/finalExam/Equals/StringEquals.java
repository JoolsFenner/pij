public class StringEquals{
	
	private void launch(){
		Person p1 = new Person("Mike");
		Person p2 = new Person("Mike");
		Person p3 = p1;
		System.out.println(p1 == p2);  // false
		System.out.println(p1 == p3); // true because pointing at same memory address
		System.out.println(p2 == p3); // false
		System.out.println(p1.equals(p2)); // true <- based on equals method and how its implemented.  You could say assuming .equals has not been overridden.
		System.out.println(p1.equals(p3)); // true
	}
	public static void main(String[] args){
		new StringEquals().launch();
	}
}
