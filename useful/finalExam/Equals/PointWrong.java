public class PointWrong{
	private final int x;
	private final int y;

	public PointWrong(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	// WRONG definition of equals
		public boolean equals(PointWrong other) {
			return (this.getX() == other.getX() && this.getY() == other.getY());
		}
	
	
	
}