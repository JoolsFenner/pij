public class Point{
	private final int x;
	private final int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	//CORRECT definition, this overrides the Object equals method
		@Override
		public boolean equals(Object o ) {
			if(o == null) {
			//check null;
				return false;
			}
			if(!(o instanceof Point)) {
			//check type
				return false;
			}
			// cast
			Point other = (Point) o; 
			//return
			return this.getX() == other.getX() && this.getY() == other.getY();
		}
	
	
	
}