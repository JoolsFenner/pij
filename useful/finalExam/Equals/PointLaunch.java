/*
*1 What went wrong? In fact, the version of equals given previously does not override the standard method equals, because its type is different. Because the equals method in Point takes a Point instead of an Object as an argument, it does not override equals in Object. Instead, it is just an overloaded alternative. Overloading in Java is resolved by the static type of the argument, not the run-time type. So as long as the static type of the argument is Point, the equals method in Point is called. However, once the static argument is of type Object, the equals method in Object is called instead. This method has not been overridden, so it is still implemented by comparing object identity
http://www.artima.com/lejava/articles/equality.html
*/

public class PointLaunch{
	
	private void PointWrongLaunch(){
		PointWrong p1 = new PointWrong(1, 2);
		PointWrong p2 = new PointWrong(1, 2);
		PointWrong q = new PointWrong(2, 3);

		System.out.println(p1.equals(p2)); // prints true
		System.out.println(p1.equals(q)); // prints false
		
		Object p2a = p2;
		System.out.println(p1.equals(p2a)); // prints false	
	}	
	
	private void PointCorrectLaunch(){
		Point p1 = new Point(1, 2);
		Point p2 = new Point(1, 2);
		Point q = new Point(2, 3);

		System.out.println(p1.equals(p2)); // prints true
		System.out.println(p1.equals(q)); // prints false
		
		Object p2a = p2;
		System.out.println(p1.equals(p2a)); // prints true
		
		
	}

	public static void main(String[] args){
		new PointLaunch().PointWrongLaunch();
		System.out.println();
		System.out.println();
		new PointLaunch().PointCorrectLaunch();
	}

}