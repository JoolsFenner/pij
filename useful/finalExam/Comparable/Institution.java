public class Institution<T> implements Iterable<T>{
	private set<T> members = new HashSet<>();
	
	//other methods
	public Iterator<T> iterator(){
		return members.iterator();
	}
}