// http://docs.oracle.com/javase/tutorial/java/data/autoboxing.html
public class IntVsInteger{

	public static void main(String[] args) {
		int simple = 1;
		Integer boxed = new Integer(1);
		
		int[] test = new int[2];
		test[0] = test[1] = 1;
		
		fiddle(simple, boxed, test);
		
		System.out.println(simple); // prints 1
		System.out.println(boxed);	// WHY DOES THIS PRINT 1, AUTO UNBOXING??
		System.out.println(test[0] + "," + test[1]); // prints 2
	
	}
	
	public static void fiddle(int simple, Integer boxed, int[] test) {
		simple = simple + 1;
		boxed = boxed + 1;
		test[0] = test[1] = 2;
		System.out.println(simple);
		System.out.println(boxed);
		System.out.println(test[0] + "," + test[1]);
	}
	
}