/*The value 0 if the argument is a string lexicographically equal to this string; a value less than 0 if the argument is a string lexicographically greater than this string; and a value greater than 0 if the argument is a string lexicographically less than this string. */

public class StringQuestion{
	
	
	public static String stringConcat(String str, char newChar){
		str = str + newChar;
		return str;
	}
	
	public static int numberCharacters(String str){
		return str.length();
	}
	
	public static int whichIsFirst(String firstString, String secondString){
		int result = firstString.compareTo(secondString);
		switch(result){
			case -1:	System.out.println(firstString + " occurs first lexiographically");
						break;
			case 0:		System.out.println("The strings are lexiographically equal");
						break;
			case 1:		System.out.println(secondString + " occurs first lexiographically");
						break;
		}
		return result;
	}
	
	public static boolean stringEquals(String firstString, String secondString){
		return firstString.equals(secondString);
	}
	
	public static void main(String[] args){
		String s1 = "string1";
		String s2 = new String("string1");
		stringConcat(s1, 'x');
		numberCharacters(s2);
		System.out.println(whichIsFirst("caa", "bbb"));
		
		System.out.println(stringEquals(s1, "stringx"));
	}

}