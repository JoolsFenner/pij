import java.util.concurrent.Semaphore;

public class ConcurrentLaunch {
	public static final Semaphore Sem = new Semaphore(1);
	public void launch(){
		Concurrent c = new Concurrent();
		
		for(int i=0; i < 50; i++){
			Thread t = new Thread(c);
			t.start();
		}
	
	}

	public static void main(String[] args){
		new ConcurrentLaunch().launch();
	}
}