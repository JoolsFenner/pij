public class Concurrent implements Runnable{

	private int number;
	
	//	public  void incrementNumber(){
	public void incrementNumber(){
			number++;
			System.out.println(number);
	}

	public void run(){
	try{
		ConcurrentLaunch.Sem.aquire();
		incrementNumber();
	}catch(Exception ex){
	}finally{
		Sem.release();
	}
		
}