public class PrimeTest{
	
	public static boolean prime(int n){
		return prime(n, n-1);
	}
	
	private static boolean prime(int n, int x){

		if(x==1) return true;
		if(n % x == 0) return false;
		return prime(n, x-1);	
	}
	
	
	public static void main(String[] args){
		System.out.println(prime(4));
	}
}