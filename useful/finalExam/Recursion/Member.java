public class Member <T>{

	public boolean member(T item, List<T> list){
		if(list.isEmpty()) return false;
		if(item.equals(list.get(0))) return true;
		member(item, list.sublist(1, list.size()));
		return false;
	}
}