public class Pal{

	public boolean Palindrome(String s){
		if(s.length() < 2 ) return true;
		
		if(s.charAt(0) != s.charAt(s.length()-1)) return false;
		
		return Palindrome(s.substring(1,s.length() -1));
	}
	
	

	private void launch(String s){
		System.out.println(Palindrome(s));
	}
	
	public static void main(String[] args){
		new Pal().launch(args[0]);
	}

}
