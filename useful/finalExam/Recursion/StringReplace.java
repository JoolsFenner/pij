public class StringReplace{
	
	public static String Replace(String s, char charToReplace, char newChar){
		
		if(s.isEmpty()) return s;
		
		if(s.charAt(0) == charToReplace) s = newChar + s.substring(1);
		
		return s.charAt(0) +  Replace(s.substring(1), charToReplace, newChar);

	}
	
	public static void main(String[] args){
		System.out.println(Replace("Java", 'a', 'e'));
	}
}