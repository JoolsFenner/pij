public class WordReverse{

	public static String reverseSentence(String str){
		
		if(str.isEmpty()) return str;
			
		int spaceIndex = 0;
		
		if(str.lastIndexOf(' ') != -1 ) spaceIndex = str.lastIndexOf(' ');
		
		return str.substring(spaceIndex, str.length()-1) + " " + reverseSentence(str.substring(0, spaceIndex));
		
	}
	
	public static void main(String[] args){
		System.out.println(reverseSentence("Jools Loves Java"));
	}
}