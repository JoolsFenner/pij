public class Fib{

	public int Fibonacci(int x){
		if(x == 1 || x == 2){
			return 1;
		} else{
			return Fibonacci(x - 1) + Fibonacci(x - 2);
		}
	
	}

	private void launch(int y){
		System.out.println(Fibonacci(y));
	}
	
	public static void main(String[] args){
		new Fib().launch(Integer.parseInt(args[0]));
	}

}