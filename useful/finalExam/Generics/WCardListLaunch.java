import java.util.List;
import java.util.ArrayList;

public class WCardListLaunch{

	private void launch(){
		List<String> stringList = new ArrayList<String>();
		stringList.add("one");
		stringList.add("twp");
		stringList.add("three");
		
		List<Integer> integerList = new ArrayList<Integer>();
		integerList.add(1);
		integerList.add(2);
		integerList.add(3);
		
		WildCardList wcTest = new WildCardList();
		System.out.println(wcTest.getHead(stringList));
		System.out.println(wcTest.getHead(integerList));
	}
	
	public static void main(String[] args){
		new WCardListLaunch().launch();
	}
}