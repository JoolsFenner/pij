//int and Integer are different as shown by this....

public class IntVsInteger{
	public void printTest(int number){
		System.out.println("int type");
	}
	public void printTest(Integer number){
		System.out.println("Integer type");
	}
	
	private void launch(){
		printTest(1);
		printTest(new Integer(1));
	}
	
	public static void main(String[] args){
		new TraceQuestion().launch();
	}
	
}