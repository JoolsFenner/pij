import java.util.List;
import java.util.ArrayList;

public class GListLaunch{

	private void launch(){
		List<String> stringList = new ArrayList<String>();
		stringList.add("one");
		stringList.add("twp");
		stringList.add("three");
		
		List<Integer> integerList = new ArrayList<Integer>();
		integerList.add(1);
		integerList.add(2);
		integerList.add(3);
		
		GenericList<Integer> stringTest = new GenericList<Integer>(); //WHY CAN THIS BE INTEGER OR STRING YET BOTH WORK BELOW
		System.out.println(stringTest.getHead(stringList));
		System.out.println(stringTest.getHead(integerList));
	}
	
	public static void main(String[] args){
		new GListLaunch().launch();
	}
}