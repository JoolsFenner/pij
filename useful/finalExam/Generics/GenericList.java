import java.util.List;

public class GenericList<T>{

	public <T> T getHead(List<T> list){
		return list.get(0);
	}

}