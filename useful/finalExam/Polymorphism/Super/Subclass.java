public class Subclass extends Superclass{

	@Override
	public void printSomething(){
		super.printSomething();
		System.out.println("Printing from Subclass");
	}
}