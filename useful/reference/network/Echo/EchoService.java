/*1. Define the interface of the service.
2. Implement the service.
3. Compile the server.
4. Compile the stub with rmic
5. Write a launcher that binds the server to the registry, and execute it.
*/

import java.rmi.Remote;
import java.rmi.RemoteException;
/**
* An implementation of the echo service.
*/
public interface EchoService extends Remote {
	/**
	* Returns the same string passed as parameter
	* @param s a string
	* @return the same string passed as parameter
	*/
	public String echo(String s) throws RemoteException;
}