public class Recursion{

	public boolean palindromeCheck(String str){
		if(str.length() < 2){
			System.out.println("true");
			return true;
		}
		char first = str.charAt(0);
		char last = str.charAt(str.length()-1);
		
		if (first != last){
			System.out.println("false");
			return false;
		}
		
		return palindromeCheck(str.substring(1, str.length()-1));
	
	}

	public static void main(String[] args){
		new Recursion().palindromeCheck(args[0]);
	}
}