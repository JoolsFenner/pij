public class Enumeration{
	
	EnumDay day;
	
	public Enumeration (EnumDay day){
		this.day = day;
	}
	
	public static void main(String[] args){
		Enumeration e = new Enumeration(EnumDay.SATURDAY);
		e.tellItLikeItIs();	
	}
	
	public void tellItLikeItIs() {
		switch (day) {
			case MONDAY:
			System.out.println("Mondays are bad.");
			break;
			
		case FRIDAY:
			System.out.println("Fridays are better.");
			break;
				 
		case SATURDAY: case SUNDAY:
			System.out.println("Weekends are best.");
			break;
				
		default:
			System.out.println("Midweek days are so-so.");
			break;
		}
	}
}