import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalendarTest{
		
	public void launch(String[] date){
		Calendar userDate = new GregorianCalendar();
		int year = Integer.parseInt(date[0]);
		int month = Integer.parseInt(date[1])-1; //Calendar month is zero indexed
		int day = Integer.parseInt(date[2]);
		userDate.set(year,month,day);
		System.out.println(userDate.getTime());
		checkDateInFuture(userDate);
	}
	
	public void checkDateInFuture(Calendar userDate){
	Calendar now = Calendar.getInstance();
	if(userDate.before(now)) {
		System.out.println("error");	
	} else {
		System.out.println("date is in future");	
	}
		
	}
	public static void main(String[] args){
		new CalendarTest().launch(args);
	}	
}