import java.lang.reflect.*;

public class Reflection{
	public static void main(String[] args){
		new Reflection().launch();
	}
	
	private void launch(){
		Person p1 = new Person("julian", 33, "male");
		Person p2 = new Person("andrea", 36, "female");
		
		getObject(p2);
	}

	public void getObject(Object obj) {
		System.out.println(obj.getClass());
		for (Field field : obj.getClass().getDeclaredFields()) {
			field.setAccessible(true); // if you want to modify private fields
			try{
				System.out.print(field.get(obj)+ ",");
			} catch(IllegalAccessException ex){
				System.out.println(ex);
			}
			
		}
		System.out.println();
	}
}

