public class Break{

	public static void main(String[] args){
		new Break().launch();
	}
	
	private void launch(){
		int x = 10;
		
		for(int i=0; i < x; i++){
			if(i == 5) break;
			System.out.println(i+1);
		}
		//breaks to here
	}
	
}
