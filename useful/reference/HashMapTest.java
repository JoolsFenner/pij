import java.util.Map;
import java.util.HashMap;

public class HashMapTest {
	private Map<Integer, String> joolsMap;
	
	private void launch(){	
		joolsMap = new HashMap<Integer, String>();
		joolsMap.put(1, "one");
		joolsMap.put(2, "two");
		joolsMap.put(3, "three");
		System.out.println(joolsMap.get(1));
	}


	public static void main(String[] args){
		new HashMapTest().launch();
	}
}