import java.util.ArrayList;

public class DownCast{
	public void makeThemBark(ArrayList<Animal> animals) {
		for (int i = 0; i < animals.size(); i++) {
			Animal nextAnimal = animals.get(i);
			Dog dog = (Dog) nextAnimal;
			dog.bark();
		}	
	}
	
	
	private void launch(){
		ArrayList<Animal> animalList = new ArrayList<Animal>();
		/*if a cat is added to the list the makeThemBark method will throw a runtime error*/
		//animalList.add(new Cat("Cat1"));
		animalList.add(new Dog("Dog1"));
		animalList.add(new Dog("Dog2"));
		animalList.add(new Dog("Dog3"));
		
		makeThemBark(animalList);
		
		
	}
	public static void main(String[] args){
		new DownCast().launch();
	}
}