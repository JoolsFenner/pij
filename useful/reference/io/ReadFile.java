import java.io.*;

public class ReadFile{
	public static void main(String[] args){
		new ReadFile().launch();
	}
	
	private void launch(){
		String fileName1 = "data.txt";
		String fileName2 = "junk.txt";
		System.out.println(fileName1 + " exists: " + fileExists(fileName1));
		System.out.println(fileName2 + " exists: " + fileExists(fileName2));
		System.out.println();
		
		readFileContents("data.txt");
		
	}
	
	public boolean fileExists(String filename){
		File file = new File(filename);
		return file.exists();
	}
	
	public void readFileContents(String filename){
		File file = new File(filename);
		BufferedReader in = null;
		try{	
			in = new BufferedReader(new FileReader(file));
			String line;
			while ((line = in.readLine()) != null) {
				// normally you don't mix assignment and conditional clauses in same line but its used in this particular case as its so common
				System.out.println(line);
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex){
			ex.printStackTrace();
		} finally{
			closeReader(in);
		}	
	}
	
	private void closeReader(Reader reader){
		try{
		if(reader != null){
			reader.close();
		}
		} catch(IOException ex){
			ex.printStackTrace();
		}
	}

}