import java.io.*;

public class WriteFile{
	public static void main(String[] args){
		
		new WriteFile().launch();
	}
	
	private void launch(){
		writeFile("data2.txt");
		
	}
	
	
	public void writeFile(String filename){
		File file = new File(filename);
		PrintWriter out = null;
		try {
			out = new PrintWriter(file);
			out.write("hello world, hello world, hello world, hello world, hello world, hello world, ");
		} catch (FileNotFoundException ex) {
			// This happens if file does not exist and cannot be created,
			// or if it exists, but is not writable
			System.out.println("Cannot write to file " + file + ".");
		} finally {
			out.close();
		}	
	}
}