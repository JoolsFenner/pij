import java.io.*;
import java.util.ArrayList;


public class WriteObject{
	
	private void launch(){
		ArrayList<Person> personList = new ArrayList<Person>();
		
		Person person1 = new Person("Julian", 33, "male");
		Person person2 = new Person("Andrea", 36, "female");
		Person person3 = new Person("Graciela", 68, "female");

		personList.add(person1);
		personList.add(person2);
		personList.add(person3);
		
		writeFile(personList);
	}
	
	

	public void writeFile(ArrayList<Person> list){
		File file = new File("PersonData.txt");
		PrintWriter out = null;
		try {
			out = new PrintWriter(file);
			
			for(Person next : list){
				out.write(next.getClass() + "," + next.getName()+ "," + next.getAge() + "," + next.getSex());
				out.write(System.getProperty("line.separator"));
			}
			
		} catch (FileNotFoundException ex) {
			// This happens if file does not exist and cannot be created,
			// or if it exists, but is not writable
			System.out.println("Cannot write to file " + file + ".");
		} finally {
			out.close();
		}	
	}
	
	public static void main(String[] args){	
		new WriteObject().launch();
	}
	
	
	
}