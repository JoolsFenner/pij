import java.io.*;
import java.util.ArrayList;


public class ReloadObject{
	
	private void launch(){
		ArrayList<Person> personList = new ArrayList<Person>();
		dataManager();
		Person person1 = new Person("Julian", 33, "male");
		Person person2 = new Person("Andrea", 36, "female");
		Person person3 = new Person("Graciela", 68, "female");

		personList.add(person1);
		personList.add(person2);
		personList.add(person3);
		
		writeFile(personList);
		
		System.out.println("Proceed?");
		System.console().readLine();
		
		Person person4 = new Person("Dave", 3, "male");
		Person person5 = new Person("Linda", 6, "female");
		Person person6 = new Person("Gary", 8, "male");
		
		personList.add(person4);
		personList.add(person5);
		personList.add(person6);
		
		writeFile(personList);
		
	}
	
	
	public void dataManager(){
		if (fileExists("PersonData.txt")){
			readFileContents("PersonData.txt");
		}
	}
	
	public void readFileContents(String filename){
		File file = new File(filename);
		BufferedReader in = null;
		try{	
			in = new BufferedReader(new FileReader(file));
			String line;
			while ((line = in.readLine()) != null) {
				String[] lineArray = line.split(",");
				Person person = new Person();
				
				person.setName(lineArray[1]);
				person.setAge(Integer.parseInt(lineArray[2]));
				person.setSex(lineArray[3]);
				
				//personList.add(person);
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex){
			ex.printStackTrace();
		} finally{
			closeReader(in);
		}
	}
	
	private void closeReader(Reader reader){
		try{
		if(reader != null){
			reader.close();
		}
		} catch(IOException ex){
			ex.printStackTrace();
		}
	}
	
	public void writeFile(ArrayList<Person> list){
		
		File file = new File("PersonData.txt");
		PrintWriter out = null;
		try {
			out = new PrintWriter(file);
			
			for(Person next : list){
				out.write(next.getClass() + "," + next.getName()+ "," + next.getAge() + "," + next.getSex());
				out.write(System.getProperty("line.separator"));
			}
			
		} catch (FileNotFoundException ex) {
			// This happens if file does not exist and cannot be created,
			// or if it exists, but is not writable
			System.out.println("Cannot write to file " + file + ".");
		} finally {
			out.close();
		}
	}
	
	public boolean fileExists(String filename){
		File file = new File(filename);
		return file.exists();
	}
	
	public static void main(String[] args){	
		new ReloadObject().launch();
	}
}