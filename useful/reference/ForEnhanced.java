import java.util.ArrayList;

public class ForEnhanced{

	
	public static void main(String[] args){
		new ForEnhanced().launch();
	}
	
	private void launch(){
		ArrayList<Integer> intList = new ArrayList<Integer>();
		intList.add(1);
		intList.add(3);
		intList.add(5);
		
		for(Integer next : intList){
			System.out.println(next);
		}
		
	}
	
}
