/*

Static - for class methods that can be used without instantiation.  Cannot access non-static methods of same class

final - cannot be overridden in sub class*/

//both can be used together for creating a constant

public class MaxSeconds {
   public static final int MAX_SECONDS = 25;
}

