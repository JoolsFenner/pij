public class Constant {
	public static final int CONSTANT_LIMIT = 5;
	public static int constantCount;
	
	public Constant(){	
		constantCount++;
		if(constantCount > CONSTANT_LIMIT){
			System.out.println("Number of Constant objects is " + constantCount + ". This is greater than the limit of " + CONSTANT_LIMIT);
		}
	}
	
	public static void main(String[] args){
		new Constant().launch();
	}
	
	private void launch(){
		Constant c2 = new Constant();
		Constant c3 = new Constant();
		Constant c4 = new Constant();
		Constant c5 = new Constant();
		Constant c6 = new Constant();
	}
}