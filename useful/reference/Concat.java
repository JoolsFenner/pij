/*
note difference between numbers at front of print statement and those after strings.  First numbers 
are added and others are concatented.

output is 3 blind mice 12 blind mice

*/

public class Concat{

	private void launch(){
		System.out.println(1 + 2 + " blind mice " + 1 + 2 + " blind mice");
	}

	public static void main(String[] args){
		new Concat().launch();
	}
	
}