public class Switch{
	public static void main(String[] args){
		new Switch().launch(3);
	}
	
	private void launch(int x){
		switch(x % 2){
			case 0: System.out.println("even");
				break;
			case 1: System.out.println("odd");
				break;
			default: System.out.println("error");
				break;
		}
	}
}