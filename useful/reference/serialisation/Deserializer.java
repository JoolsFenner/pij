import java.util.ArrayList;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
 
public class Deserializer{
 
   public static void main (String args[]) {
 
	   Deserializer deserializer = new Deserializer();
	   ArrayList<Address> addressList = deserializer.deserialzeAddressList();
	   System.out.println(addressList);
   }
 
   public ArrayList<Address> deserialzeAddressList(){
 
	   ArrayList<Address> addressList;
 
	   try{
 
		   FileInputStream fin = new FileInputStream("address.ser");
		   ObjectInputStream ois = new ObjectInputStream(fin);
		   addressList = (ArrayList<Address>) ois.readObject();
		   ois.close();
 
		   return addressList;
 
	   }catch(Exception ex){
		   ex.printStackTrace();
		   return null;
	   } 
   } 
}