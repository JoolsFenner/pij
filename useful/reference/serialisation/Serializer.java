import java.util.ArrayList;
 
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
 
public class Serializer {
 
   public static void main (String args[]) {
 
	   Serializer serializer = new Serializer();
	   serializer.serializeAddress();
   }
 
   public void serializeAddress(){
 
	   Address address = new Address();
	   address.setStreet("street1");
	   address.setCountry("country1");
	   Address address2 = new Address();
	   address.setStreet("street2");
	   address.setCountry("country2");
	   
	   ArrayList<Address> addressList = new ArrayList<Address>();
	   addressList.add(address);
	   addressList.add(address2);
 
	   try{
 
		FileOutputStream fout = new FileOutputStream("address.ser");
		ObjectOutputStream oos = new ObjectOutputStream(fout);   
		oos.writeObject(addressList);
		oos.close();
		System.out.println("Done");
 
	   }catch(Exception ex){
		   ex.printStackTrace();
	   }
   }
}