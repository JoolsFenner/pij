public class PersonLauncher{
	public static void main(String[] args){
		new PersonLauncher().launch();
	}
	
	private void launch(){
		Person Julian = new AdultPerson("Julian Fenner", 7, true);
		
		System.out.println(Julian.talk("hello world"));
		System.out.println(Julian.move(10));
		
		Person Sarah = new ChildPerson("Sarah Smith", 7, false);
		
		System.out.println(Sarah.talk("hello world"));
		System.out.println(Sarah.move(10));
		
	}

}