/**
* A person is defined by movement (as opposed to plants)
* and by speech (as opposed to animals).
*/
public interface Person {
	/**
	* Move a distance in a straight line, given in meters.
	*/
	int move(int distance);
	
	/**
	* Say a sentence.
	*/
	String talk(String words);

}