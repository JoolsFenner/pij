public class ChildPerson implements Person{
	String name;
	int position;
	boolean canTalk;
	ChildPerson(String name, int position, boolean canTalk){
		this.name = name;
		this.position = position;
		this.canTalk = canTalk;
	}
	
	
	public int move(int distance){
		position += distance;
		return position;
	}
	
	public String talk(String words){
		if(canTalk){
			return words;
		} else{
			return "cries";	
		}
	}

}