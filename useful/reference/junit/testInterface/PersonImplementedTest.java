import org.junit.*;
import static org.junit.Assert.*;

/*
set classpath=I:/lib/junit-4.11.jar;I:/lib/hamcrest-core-1.3.jar; %classpath%

javac -cp .;/lib/junit-4.11.jar HashUtilitiesTest.java

java org.junit.runner.JUnitCore HashUtilitiesTest

*/	

public class PersonImplementedTest {
	@Test
	public void testMoveChild() {
		Person p = new ChildPerson("Brian", 3, true);
		int input = 3;
		int output = p.move(input);
		int expected = 6;
		assertEquals(output, expected);
	}
	
	@Test
	public void testMoveInjuredAdult() {
		Person p = new AdultPerson("Brian", 3, true);
		int input = 4;
		int output = p.move(input);
		int expected = 5;
		assertEquals(output, expected);
	}
	
	@Test
	public void testMoveChildCantTalk() {
		Person p = new ChildPerson("Brian", 3, false);
		String input = "hello world";
		String output = p.talk(input);
		String expected = "cries";
		assertEquals(output, expected);
	}
}