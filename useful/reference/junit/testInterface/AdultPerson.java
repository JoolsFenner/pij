public class AdultPerson implements Person{
	String name;
	int position;
	boolean injured;
	AdultPerson(String name, int position, boolean injured){
		this.name = name;
		this.position = position;
		this.injured = injured;
	}
	
	
	public int move(int distance){
		
		if(injured){
			distance = distance / 2;
		}
		
		position += distance;
		
		return position;
	}
	
	public String talk(String words){
		return words;
	}

}