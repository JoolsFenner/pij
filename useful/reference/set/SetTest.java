import java.util.Set;
import java.util.HashSet;

public class SetTest{
	Set<People> peopleSet;
	public SetTest(){
		peopleSet = new HashSet<People>();
	}

	
	
	private void launch(){
		People jools = new People("Julian", 33);
		People david = new People("Davis", 68);
		System.out.println(peopleSet.add(jools));
		System.out.println(peopleSet.add(jools));
		System.out.println(peopleSet.add(david));
		System.out.println(peopleSet);
	}
	
	public static void main(String[] args){
		new SetTest().launch();
	}
}