/*
note how a println following a print behaves

*/

public class PrintLine{

	private void launch(){
		System.out.print("hello");
		System.out.println("world");
		
		System.out.println();
		
		System.out.println("hello");
		System.out.print("world");
		
		
	}

	public static void main(String[] args){
		new PrintLine().launch();
	}
	
}