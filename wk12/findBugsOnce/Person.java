public class Person{
	String s;
	public Person(){
		s = "ss";
	}
	public String getInitials(String fullName) {		
		//gets rid of double space within string
		fullName = fullName.replaceAll(" +", " ");
		//
		fullName = fullName.trim();
		
		
		String[] words = fullName.split(" ");
		
		String result = "";
		for (int i = 0; i < words.length; i++) {
			String nextInitial = "" + words[i].charAt(0);
			result = result + nextInitial.toUpperCase();
		}
		return result;
	}
}