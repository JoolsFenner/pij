import org.junit.*;
import static org.junit.Assert.*;

/*
set classpath=I:/lib/junit-4.11.jar;I:/lib/hamcrest-core-1.3.jar; %classpath%

javac -cp .;/lib/junit-4.11.jar PersonTest.java
javac -cp .;C:/Jools/lib/junit-4.11.jar PersonTest.java

java org.junit.runner.JUnitCore PersonTest

*/	

public class PersonTest {
	@Test
	public void testsNormalName() {
		Person p = new Person();
		String input = "Dereck Robert Yssirt";
		String output = p.getInitials(input);
		String expected = "DRY";
		assertEquals(output, expected);
	}
	
	@Test
	public void PersonTestDoubleSpace() {
		Person p = new Person();
		String input = "Dereck  Robert Yssirt";
		String output = p.getInitials(input);
		String expected = "DRY";
		assertEquals(output, expected);
	}
}