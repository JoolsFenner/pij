public class TreeIntSet implements IntSet {
	private int value; 	
	private TreeIntSet left;
	private TreeIntSet right; 
	
	public TreeIntSet(int value){
	//constructor
		this.value=value;
		this.left=null;
		this.right=null;
	}
	
	public void add(int newNumber) {
	//add number to tree
		if(contains(newNumber)){
		//check if newNumber already in tree, if so do not add
			System.out.println(newNumber + " is already in tree, cannot be added");
				return;
		}
			if (newNumber > this.value) {
				if (right == null) {
					right = new TreeIntSet(newNumber);
				} else {
					right.add(newNumber);
			}
		} else {
			if (left == null) {
				left = new TreeIntSet(newNumber);
			} else {
				left.add(newNumber);
			}
		}
	}
	
	public boolean contains(int number){
		if (number == this.value) {
			return true;
		} else if (number > this.value) {
			if (right == null) {
				return false;
			} else {
				return right.contains(number);
			}
		} else {
			if (left == null) {
				return false;
			} else {
				return left.contains(number);
			}
		}
	}
	
	public void containsVerbose(int number){
		if(contains(number)){
			System.out.println();
			System.out.println(number + " is already in the set");
		} else{
			System.out.println(number + " is not in the set");
		}
	}
	
	public void setToString(){
		setToString(this);
	}
	private void setToString(TreeIntSet node){;
		if(node== null){
			return;
		} else{
			setToString(node.left);
			setToString(node.right);
			System.out.print(node.value + ", ");
		}			
	}	
}	