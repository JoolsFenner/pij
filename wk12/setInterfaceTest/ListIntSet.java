public class ListIntSet implements IntSet {
	private int value;
	public ListIntSet next;
	public ListIntSet(int value){
		this.value = value;
		this.next=null;
	}
	public ListIntSet(){
		this.next=null;
	}

	public void add(int number){
		if (contains(number)){
			System.out.println("error " + number + " already in list");
			return;
		}
		
		if(this==null){
			System.out.println("error");
			return;
		} 
		
		if(this.next==null){
			this.setNext(number);
		} else{
			this.next.add(number);
		}
	}
	private void setNext(int number){
		ListIntSet newNode = new ListIntSet(number);
		next=newNode;
	}

	public boolean contains(int number){
		ListIntSet aux = this;
		
		do{
			//System.out.println(aux.value);
			if(aux.value==number){
				return true;
			}
			aux = aux.next;
		} while(aux != null);
		return false;
	}

	public void containsVerbose(int number){
	
	}

	public void setToString(){
		System.out.println(this.value);
		
		if(this.next !=null){
			this.next.setToString();
		}
	
	}
}
