import org.junit.*;
import static org.junit.Assert.*;

	
public class IntSetTest {
	//needs test added for tree implementation
	
	@Test
	public void testAdd_ListInt() {
		ListIntSet s = new ListIntSet();
		s.add(3);
		s.add(4);
		s.add(5);
		s.add(6);
		s.add(7);
		int sizeExpected = 5;
		
		int actualSize = 0;
		ListIntSet aux = s;
		do{
			actualSize++;
			aux = aux.next;
		} while(aux.next != null);
		
		assertEquals(sizeExpected, actualSize);
	}
	
	@Test
	public void testAddDupe_ListInt() {
		ListIntSet s = new ListIntSet();
		s.add(3);
		s.add(3);
		s.add(3);
		s.add(4);
		int sizeExpected = 2; //because its a set so 3 only added once
		
		int actualSize = 0;
		ListIntSet aux = s;
		do{
			actualSize++;
			aux = aux.next;
		} while(aux.next != null);
		
		assertEquals(sizeExpected, actualSize);
	}
	
	@Test
	public void testContainsTrue_ListInt() {
		ListIntSet s = new ListIntSet();
		int input = 103; 
		s.add(3);
		s.add(56);
		s.add(input);
		s.add(102);
		
		boolean result = s.contains (input);
		boolean resultExpected = true;
		
		assertEquals(resultExpected, result);
	}
	
}