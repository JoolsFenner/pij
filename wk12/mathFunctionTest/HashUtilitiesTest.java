import org.junit.*;
import static org.junit.Assert.*;

/*
set classpath=I:/lib/junit-4.11.jar;I:/lib/hamcrest-core-1.3.jar; %classpath%

javac -cp .;/lib/junit-4.11.jar HashUtilitiesTest.java

java org.junit.runner.JUnitCore HashUtilitiesTest

*/	

public class HashUtilitiesTest {
	@Test
	public void testShortHash() {
		String input = "Java";
		int output = HashUtilities.shortHash(input);
		int expected = 506;
		assertEquals(output, expected);
	}
	public void testShortHash2() {
		String input = "Java";
		int output = HashUtilities.shortHash(input);
		int expected = 507;
		assertEquals(output, expected);
	}
}