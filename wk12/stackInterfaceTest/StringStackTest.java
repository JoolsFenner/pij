import org.junit.*;
import static org.junit.Assert.*;

/*
set classpath=I:/lib/junit-4.11.jar;I:/lib/hamcrest-core-1.3.jar; %classpath%

javac -cp .;/lib/junit-4.11.jar StringStackTest.java
javac -cp .;C:/jools/lib/junit-4.11.jar StringStackTest.java

java org.junit.runner.JUnitCore StringStackTest

*/	

public class StringStackTest {
	//TEST POP EMPTY
	
	@Test
	public void testPointerPopEmpty() {
		StringStack s = new PointerStringStack();
		String input = null;
		String output = s.pop();
		String expected = null;
		assertEquals(output, expected);
	}
	
	@Test
	public void testArrayPopEmpty() {
		StringStack s = new ArrayStringStack();
		String input = null;
		String output = s.pop();
		String expected = null;
		assertEquals(output, expected);
	}
	
	
	
	//TEST PEEK
	@Test
	public void testPointerPeek() {
		StringStack s = new PointerStringStack();
		String input = "jools";
		s.push(input);
		String output = s.peek();
		String expected = "jools";
		assertEquals(output, expected);
	}
	
	@Test
	public void testArrayPeek() {
		StringStack s = new ArrayStringStack();
		String input = "jools";
		s.push(input);
		String output = s.peek();
		String expected = "jools";
		assertEquals(output, expected);
	}
	
}