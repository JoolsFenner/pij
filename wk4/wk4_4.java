import java.util.Scanner;
public class wk4_4
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
System.out.print("please enter a number: ");
String userInput = scanner.next();

System.out.print("What type of number did you input.  1 for decimal, 2 for binary: ");
String str = scanner.next();
System.out.println("");
int numberType = Integer.parseInt(str);

if (numberType == 1){
	decimal2binary(userInput);	
} else if(numberType == 2){
	binary2decimal(userInput);	
} else {
	System.out.print("invalid input");
}





} private static void decimal2binary(String userInput){
	
	int decimalNumber = Integer.parseInt(userInput);
	String binaryStringTemp = "";
	
	while(decimalNumber > 0){
		
		if(decimalNumber % 2 != 0){
			binaryStringTemp += "1";
		} else {
			binaryStringTemp += "0";
		}
		
		decimalNumber = decimalNumber/2;
	
	}
	
	int endChar = binaryStringTemp.length()-1;
	String binaryString = "";
	
	for(int i = 0; i < binaryStringTemp.length(); i++){
	
		binaryString += binaryStringTemp.charAt(endChar);
		endChar --;
	
	}
	
	System.out.print(binaryString);
	
}

 private static void binary2decimal(String userInput){

	double result = 0;
	int currentExponent = 0;

	for(int i=0; i < userInput.length(); i++){
		
		if(userInput.charAt(i)=='1'){
			currentExponent = (userInput.length()-(i+1));	
			result += (exponCalc(currentExponent));
		}
	}
	
	System.out.print(result);
} 


 private static double exponCalc(double exponent){

	int baseIncrement = 2;

	if(exponent==0){
		baseIncrement = 1;
	} else{
		for(int i = 0; i < (exponent-1); i++){
			baseIncrement= 2 * baseIncrement
			;	
		}	
	}
	
	return baseIncrement;

}
}

