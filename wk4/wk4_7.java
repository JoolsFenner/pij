import java.util.Scanner;
public class wk4_7
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
Integer2 i2 = new Integer2();
System.out.print("Enter a number: ");
String str = scanner.next();
int i = Integer.parseInt(str);
i2.setValue(i);
System.out.print("The number you entered is ");

if (i2.isEven()) {
	System.out.println("even.");
} else if (i2.isOdd()) {
	System.out.println("odd.");
} else {
	System.out.println("undefined!! Your code is buggy!)");
}


int parsedInt = Integer.parseInt(i2.toNewString());
if (parsedInt == i2.getValue()) {
System.out.println("Your toString() method seems to work fine.");
}


}}

class Integer2{private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	int value;
	
	int getValue(){
		return this.value;
	}
	void setValue(int intNew){
		this.value = intNew;
	}
	boolean isEven(){
		boolean isEvenResult;
		if(this.value % 2 == 0){
			isEvenResult = true;
			
		} else {
			isEvenResult = false;
		}
		return isEvenResult;
	}
	boolean isOdd(){
		boolean isOddResult;
		if(this.value % 2 != 0){
			isOddResult = true;
			
		} else {
			isOddResult = false;
		}
		return isOddResult;
	}
	String toNewString(){
		String str = new String("" + this.value);
		return str;
	}
}
