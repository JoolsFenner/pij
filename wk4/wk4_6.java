import java.util.Scanner;
public class wk4_6
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
//INITIALISE
Point a = new Point();
System.out.print("Enter first point's x coordinate: ");
a.x = scanner.nextDouble();
System.out.print("Enter first point's y coordinate: ");
a.y = scanner.nextDouble();

Point b = new Point();
System.out.print("Enter second point's x coordinate: ");
b.x = scanner.nextDouble();
System.out.print("Enter second point's y coordinate:  ");
b.y = scanner.nextDouble();



//OUTPUT & EXECUTION
System.out.println("");
System.out.println("First point's coordinates are " + a.x + " " + a.y);
System.out.println("Second point's coordinates are " + b.x + " " + b.y);
System.out.println("");

System.out.println("distance between points is " + b.distanceTo(a));
System.out.println("distance from first point to origin is " + a.distanceToOrigin());
System.out.println("distance from second point to origin is " + b.distanceToOrigin());
System.out.println("");

System.out.println("Changing first point's coordinates to 5,6");
a.moveTo(5,6);
System.out.println("First point's coordinates are now " + a.x + " " + a.y);
System.out.println("");

System.out.println("Moving second point to first point");
b.moveTo(a);
System.out.println("Second point's coordinates are now " + b.x + " " + b.y);
System.out.println("");

System.out.println("Cloning first point");
Point aClone = a.clonePoint();
System.out.println("Clone of first point's coordinates are  " + aClone.x + " " + aClone.y);
System.out.println("");

System.out.println("Cloning second point with opposite coordinates");
Point bCloneOpposite = b.cloneOpposite();
System.out.println("Clone of second point's opposite coordinates are " + bCloneOpposite.x + " " + bCloneOpposite.y);
System.out.println("");


//CLASS DEFINITIONS

}}

class Point {private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	double x;
	double y;
	
	//METHODS
	double distanceTo(Point otherPoint){
		double distanceSquared = ((this.x - otherPoint.x)*(this.x - otherPoint.x)+(this.y - otherPoint.y)*(this.y - otherPoint.y));
		double distanceSquareRoot = Math.sqrt(distanceSquared);
		return distanceSquareRoot;
	}
	double distanceToOrigin(){
		Point origin = new Point();
		origin.x = 0;
		origin.y = 0;
		return distanceTo(origin);
	}
	void moveTo(double x, double y){
		this.x = x;
		this.y= y;
	}
	void moveTo(Point otherPoint){
		this.x = otherPoint.x;
		this.y = otherPoint.y;
	}
	Point clonePoint(){
		Point copy = new Point();
		copy.x = this.x;
		copy.y = this.y;
		return copy;
	}
	Point cloneOpposite(){
		Point copy = this.clonePoint();
		copy.x = (copy.x * -1);
		copy.y = (copy.y * -1);
		return copy;
	}	
}
