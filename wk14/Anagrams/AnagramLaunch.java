
public class AnagramLaunch{
	
	public static void main(String[] args){
		String str = args[0];
		new AnagramLaunch().launch(str);
	}
	
	private void launch(String str){
		System.out.println("Anagrams of " + str + " are " + Anagram.getAnagrams(str));
	}

	

}