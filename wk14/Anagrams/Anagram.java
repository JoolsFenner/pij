import java.util.ArrayList;

public class Anagram{
	
	public static ArrayList<String> getAnagrams(String str){
		ArrayList<String> stringList = new ArrayList<String>();
		stringList = getAnagrams(str, str);
		return stringList;
	}
	
	private static ArrayList<String> getAnagrams(String strRecursive, String strOrig){
		ArrayList<String> stringList = new ArrayList<String>();
		
		if(strRecursive.length() <= 1){
			//System.out.println("base case " + strRecursive);
			stringList.add(strRecursive);
			//ADD LAST LETTER + OTHER PERMUTATIONS HERE
			return stringList;
		} else {		
				stringList = getAnagrams(strRecursive.substring(0,strRecursive.length()-1), strOrig);
		
				int stringListSize = stringList.size();
				for(int i = 0; i < stringListSize; i++){
					//creates duplicate of list for modification
					stringList.add(stringList.get(i));
				}
						
				String currentLetterToAdd = strOrig.substring((strRecursive.length()-1),(strRecursive.length()));
				
				stringListSize = stringList.size();
				
				for(int i = 0; i < stringListSize; i++){	
					
					//adds letters to strings
					//stringList.set(i, currentLetterToAdd + stringList.get(i));
				}
				
				System.out.println("strRecursive 2 = " + strOrig.substring((strRecursive.length()-1),(strRecursive.length())));
				System.out.println();
				stringList.add(strRecursive);
				
			//}
			
			
			return stringList;
		}
	}


}