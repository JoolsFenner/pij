//unfinished messy solution, on wrong track

import java.util.Arrays;  
import java.util.List;  
import java.util.ArrayList; 

public class BadMergeSort {
	public ArrayList<Integer> results = new ArrayList<Integer>();
	
	public void /*List<Integer>*/ mergeSort(List<Integer> inputList){
		int subListOneEnd = 0;
		int subListTwoStart = 0;
		
		if(inputList.size() <= 1){
			return; 
		} else{
			
			if(inputList.size() % 2 !=0){
				subListOneEnd = (inputList.size() /2 + 1) ;
				subListTwoStart = ((inputList.size()/2 ) +1) ;
			} else{
				subListOneEnd = ((inputList.size()/2)) ;
				subListTwoStart = (inputList.size() /2) ;
			}
				
			
			List<Integer> subListOne = new ArrayList<Integer>(inputList.subList(0, subListOneEnd));
			List<Integer> subListTwo = new ArrayList<Integer>(inputList.subList(subListTwoStart, inputList.size()));
			
			//System.out.println(subListOne + " " + subListTwo);
			
			mergeSort(subListOne);
			mergeSort(subListTwo);
			
			if(subListOne.get(0) < subListTwo.get(0)){
				results.add(subListOne.get(0));
				
			} else{
				results.add(subListTwo.get(0));
			}
		
			
		}
		
		
		
	}
	
	public static void main(String[] args)	{
		new BadMergeSort().launch();
	}
	
	private void launch()	{
		List<Integer> myList = new ArrayList<Integer>(Arrays.asList(3, 7 , 2 ,9, 1));
		mergeSort(myList);
		System.out.println(results);
	}
}