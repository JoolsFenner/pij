import java.util.concurrent.Executor;
//import java.util.concurrent.Executors;
//import java.util.concurrent.ExecutorService;

public class MyExecutor implements Executor{

	public void execute(Runnable command){
		command.run();
	}

}