import java.util.Scanner;
public class wk5_2b
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
System.out.print("Enter fibonacci number: ");
int fibInput = scanner.nextInt();

Trace fibStackTrace = new Trace();
fibStackTrace.stackPosition = 0;
fibStackTrace.computationSteps = 0;

System.out.println("");
System.out.println("Stack depth");
System.out.println("Fibonacci " +  fibInput + " = " + fib(fibInput, fibStackTrace));
System.out.println("Computation steps: " + fibStackTrace.computationSteps);

} private static int fib(int n, Trace trace) {
		trace.stackInc();
		
	if ((n == 1) || (n == 2)) {
		trace.stackDec();
		
		return 1;
	} else {
		int result = fib(n-1, trace) + fib(n-2, trace); // method calls itself
		trace.stackDec();
		return result;
	}
}



}

class Trace{private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	int stackPosition;
	int computationSteps;
	
	void stackInc(){
		this.stackPosition++;
		this.computationSteps++;
		System.out.println(this.stackPosition);
	}
	void stackDec(){
		this.stackPosition--;
		this.computationSteps++;
		System.out.println(this.stackPosition);
		
	}

}

