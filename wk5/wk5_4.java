import java.util.Scanner;
public class wk5_4
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
System.out.println("");
System.out.print("please enter word: ");
String str = scanner.next();
System.out.println("");
System.out.print("'" + str + "'" + " palindrome = " + checkPalindrome(str));
System.out.println("");

} private static boolean checkPalindrome(String word) {

	if(word.length() < 2) { 
		return true;  
	}
		char first  = word.charAt(0);
		char last   = word.charAt(word.length()-1);
	
	if( first != last ) { 	
		return false; 
	} else { 
		return checkPalindrome(word.substring(1,word.length()-1)); 
	}
}
}

