import java.util.Scanner;
public class wk5_2c
{
		private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	public static void main(String[] args)
	{
System.out.print("Please enter number of discs: ");
int discs = scanner.nextInt();
char source ='a';
char destination ='b';
char spare ='c';

Counter hanoiMoves = new Counter();
hanoiMoves.count = 0;
hanoi(discs, source, destination, spare, hanoiMoves);
System.out.println("");
System.out.println(discs + " discs moved in " + hanoiMoves.count + " moves.");

} private static void hanoi(int discs, char source, char destination, char spare, Counter hanoiMoves){

	if(discs==1){
		System.out.println("moving disc " + discs + " from peg " + source + " to peg " + destination);
		hanoiMoves.increment();
	} else{
		hanoi(discs-1, source, spare, destination, hanoiMoves);
		System.out.println("moving disc " + discs + " from peg " + source + " to peg " + destination);
		hanoiMoves.increment();
		hanoi(discs-1, spare, destination, source, hanoiMoves);
	}
	
}

}

class Counter{private static Scanner scanner=new Scanner(System.in); static{scanner.useDelimiter(System.getProperty("line.separator"));}
	int count;
	void increment(){
		this.count++;
	}
}
