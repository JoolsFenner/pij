public class BankAccountLauncher implements Runnable {
	private static BankAccount b;
	
	public BankAccountLauncher() {
	}
	public BankAccountLauncher(BankAccount bankAccount) {
		this.b = bankAccount;
	}
	
	public static void main(String args[]) {
		new BankAccountLauncher().launch();
	}
	
	private void launch(){
	
		BankAccount bankAccount = new BankAccount();
		for (int i = 0; i < 1000; i++) {
			BankAccountLauncher BankAccountLauncherTask = new BankAccountLauncher(bankAccount);
			Thread t = new Thread(BankAccountLauncherTask);
			t.start();
		}
	}
	
	public void run() {
			System.out.println("Balance at start = " + b.getBalance());
			
			b.deposit(2000);
			b.retrieve(1000);
			
			System.out.println("Balance at end   = " + b.getBalance());
		}
	
}
