public class Increaser implements Runnable {
	private static Counter c;
	
	public Increaser() {
	}
	public Increaser(Counter counter) {
		this.c = counter;
	}
	
	public static void main(String args[]) {
		new Increaser().launch();
	}
	
	private void launch(){
	
		Counter counter = new Counter();
		for (int i = 0; i < 100; i++) {
			Increaser increaserTask = new Increaser(counter);
			Thread t = new Thread(increaserTask);
			t.start();
		}
	}
	
	public void run() {
		System.out.println("Before " + c.getCount());
		for (int i = 0; i < 1000; i++) {
			c.increase();	
		}
		System.out.println("After " + c.getCount());	
	}
}
