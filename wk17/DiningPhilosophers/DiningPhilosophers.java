/**
 * DiningPhilosophers.java
 *
 * This class creates the dining server and the philosophers
 *
 * @author Greg Gagne, Peter Galvin, Avi Silberschatz
 * @version 1.0 - August 6, 1999
 * Copyright 2000 by Greg Gagne, Peter Galvin, Avi Silberschatz
 * Applied Operating Systems Concepts - John Wiley and Sons, Inc.
 */

public class DiningPhilosophers {  
   public static void main(String args[]) {
      DiningServer server = new DiningServer();

      Philosopher[] philosopherArray = new Philosopher[DiningServer.NUM_OF_PHILS];
     
     // create the philosopher threads
     for (int i = 0; i < DiningServer.NUM_OF_PHILS; i++)
         philosopherArray[i] = new Philosopher(server,i);
     
     for (int i = 0; i < DiningServer.NUM_OF_PHILS; i++)
         philosopherArray[i].start();
   }
}