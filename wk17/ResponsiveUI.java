import java.util.Scanner;
import java.util.ArrayList;

public class ResponsiveUI implements Runnable {
	private static int taskCount;
	private static ArrayList<Integer> complete = new ArrayList<Integer>();
	private static ArrayList<Integer> completeFinal = new ArrayList<Integer>();
	private int taskLength;
	private int taskNo;
	
	public ResponsiveUI(){
	}
	public ResponsiveUI(int taskLength) {
		this.taskLength = taskLength;
		taskCount++;
		taskNo = taskCount;
	}
	
	public void isComplete(){
	
		if (!complete.isEmpty()){
			System.out.println();
			
			for(int i = 0; i < complete.size(); i++){
				completeFinal.add(complete.get(i));
			}
			
			System.out.println("Finished tasks: " + completeFinal);
			complete.clear();
		}
	}
	
	public void run() {
			try{
				Thread.sleep(taskLength);
			} catch (InterruptedException ex){
				System.out.println("Interrupted Exception");
			}

			complete.add(taskNo);
			isComplete();
		}
	
	public static void main(String args[]) {
		while(taskCount < 10){		
			System.out.print("Please enter a task duration in ms ");
			Scanner sc = new Scanner(System.in);
			int userInputTime = sc.nextInt();
			ResponsiveUI r = new ResponsiveUI(userInputTime);
			Thread t = new Thread(r);
			t.start();	
		}	
	}
}
