public class IntTreeNode {
	private int value; 	
	private IntTreeNode left;
	private IntTreeNode right; 
	//static variables for use in depthStaticVar()
	private static int tempDepth;
	public static int treeDepth;
	
	public IntTreeNode(int value){
	//constructor
		this.value=value;
		this.left=null;
		this.right=null;
		this.tempDepth = 0;
		this.treeDepth = 0;
	}
	
	
	//SET
	public void add(int newNumber) {
	//add number to tree
		if (newNumber > this.value) {
			if (right == null) {
				right = new IntTreeNode(newNumber);
			} else {
				right.add(newNumber);
			}
		} else {
			if (left == null) {
				left = new IntTreeNode(newNumber);
			} else {
				left.add(newNumber);
			}
		}
	}
	
	
	//GET
	public int getValue(){
	//get this node's value
			return this.value;	
	}
	
	public int getLeftValue(){
	//get this node's left value
		if(this.left!=null){
			return this.left.value;
		} 
		return 0;	
	}
	
	public int getRightValue(){
	//get this node's right value
		if(this.right!=null){
			return this.right.value;
		} 
		return 0;
		
	}
	
	
	//METHODS
	public void getMax(){
	//get largest value in tree
		if(this.right==null){
			System.out.println("Max is " + this.value);
			return;
		} else{
			this.right.getMax();
		}
	}
	
	public void getMin(){
	//get smallest value in tree
		if(this.left==null){
			System.out.println("Min is " + this.value);
			return;
		} else{
			this.left.getMin();
		}
	}
	
	public boolean contains(int n) {
	//check whether tree contains given value
		if (n == this.value) {
			return true;
		} else if (n > this.value) {
			if (right == null) {
				return false;
			} else {
				return right.contains(n);
			}
		} else {
			if (left == null) {
				return false;
			} else {
				return left.contains(n);
			}
		}
	}
		
	public void nodesToString(int headValue){
	//prints every node as a list in square brackets containing value, left branch and right branch
		if(left != null){
			left.nodesToString(headValue);
		}
		System.out.println("[ Node Value = " + this.value);
		System.out.println("  Left branch = " + getLeftValue()); 
		System.out.println("  Right branch = " + getRightValue());
		
		if(this.value>headValue){
			System.out.print("  RIGHT TREE]");
		} else if(value<headValue) {
			System.out.print("  LEFT TREE]");
		} else {
			System.out.print("  ROOT]");
		}
		System.out.println();
		System.out.println();
		
		if(right !=null){
			right.nodesToString(headValue);
		}
			
	}
	
	public void depth() {
	//get depth using recursive method
		System.out.println("Tree depth using recursive method = " + depth(this));
	}
	private int depth(IntTreeNode node) {
		if (node==null) {
			return(0);
		}
		else {
			int lDepth = depth(node.left);
			int rDepth = depth(node.right);

			// use the larger + 1
			return(Math.max(lDepth, rDepth) + 1);
		}
	} 	
	
	
	
	/** THE METHOD BELOW IS BAD AS IT USES STATIC VARIABLES, JUST USED TO SHOW DIFFERENT WAY OF FINDING DEPTH
	*
	*
	*/
	
	public void depthStaticVar(){
	//get depth using recursive method static variables
		System.out.println("Tree depth using static var = " + depthStaticVar(this));
	}
	private int depthStaticVar(IntTreeNode node) {
		//int currentDepth, depth;
	
		if (node != null) {
			tempDepth++;

			// record total depth if current depth is greater than total depth
			if (tempDepth > treeDepth) {
				treeDepth = tempDepth;
			}

			// recursively traverse entire tree
			this.depthStaticVar(node.left);
			this.depthStaticVar(node.right);

			// decrement as we traverse up the binary tree
			
			tempDepth--;
			//System.out.println(node.value);
		}
		return treeDepth;
		
	}
	
	

}	