public class IntTreeScript{
	IntTreeNode first = null;
	
	public static void main(String[] args){
		IntTreeScript x = new IntTreeScript();
		x.launch();
	}
	
	private void launch(){
		//set start of tree
		IntTreeNode a = new IntTreeNode(49);
		first = a;
		first.add(90);
		first.add(80);
		first.add(70);
		first.add(60);
		first.add(50);
		first.add(1);
		/*first.add(2);
		first.add(3);
		first.add(4);
		first.add(5);
		first.add(6);
		first.add(6);
		first.add(8);
		first.add(9);
		first.add(30);
		first.add(20);
		first.add(10);
		first.add(0);
		8*/
		//System.out.println(first.contains(27));
		//first.getMin();
		//first.getMax();
		
		first.nodesToString(first.getValue()); 
		first.depthStaticVar();
		first.depth();
	

	}
}