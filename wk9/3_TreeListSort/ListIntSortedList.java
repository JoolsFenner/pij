public class ListIntSortedList implements IntSortedList {
	public ListNode head;		
	public ListIntSortedList(){
		head = null;
    }	
	
	public void add(int number){
		ListNode newNode = new ListNode(number);
		
		if(head==null){
			head = newNode;
			return;
		}
		
		if(newNode.value <= head.value){
			//System.out.println("TEST");
			newNode.next = head;
			head = newNode;
		} else {
			head.setNext(newNode);
		}
		
	}
	
	public boolean contains(int x){
		return true;
	}
		
	public void convertToString(){
		head.printNodes();
	}
	public void getHead(){
		System.out.println(head.value);
	}
			
}
