public class ListNode {
    public int value;
    public ListNode next;

    public ListNode(int value) {
	  this.value = value;
	  next = null;
    }

   
    public void setNext(ListNode node) {
	  //System.out.println(node.next);
		if(this.next==null){	
			next=node;
			return;
		} else if (node.value <= this.next.value){
			
			node.next = this.next;
			this.next = node;
			return;
		} 
		this.next.setNext(node);

	  
	}

    
	
	public void printNodes(){
		
		System.out.println(value);
		if(this.next!=null){
			this.next.printNodes();
		}
		
	}

   
}