public class TreeNode{
	private int value; 	
	private TreeNode left;
	private TreeNode right; 
	public TreeNode(int value){
	//constructor
		this.value=value;
		this.left=null;
		this.right=null;
	}
	
	public int getValue(){
		return value;
	}
	public TreeNode getLeft(){
		return left;
	}
	public TreeNode getRight(){
		return right;
	}
	
	public void setNext(TreeNode newNode) {
			if (newNode.value > this.value) {
				if (right == null) {
					right = newNode;
				} else {
					right.setNext(newNode);
					
			}
		} else {
			if (left == null) {
				left = newNode;
			} else {
				left.setNext(newNode);
				
			}
		}
	}
	

}