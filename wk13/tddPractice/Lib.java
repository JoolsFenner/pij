public interface Lib {
	
	
	int setNewUserId();
	
	int getId(String str);
	
	String getName();
	
	int getLibraryId();
	
	int countMembers();
	
	void addMember(LibraryUser user);
	
	int getMaxBooksPerUser();
	
	String addBook(String title, String author); //return title of book added
	
	boolean takeBook(String title);
	
	void returnBook(Book book);
	
}