public class LibraryBook implements Book{
	private final String author;
	private final String title;
	private boolean taken;
	public LibraryBook (String author, String title){
		this.author = author;
		this.title = title;
		this.taken = false;
	} 
	
	public String getAuthor(){
		return author;
	}
	
	public String getTitle(){
		return title;
	}
	
	public boolean getTaken(){
		return taken;
	}
	
	public void setTaken(boolean flag){
		this.taken = flag;
	}
	
	
}