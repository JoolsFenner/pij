
import org.junit.*;
import static org.junit.Assert.*;

/*
COMPILE
javac -cp .;C:/Jools/lib/junit-4.11.jar 

RUN
java org.junit.runner.JUnitCore HashUtilitiesTest
**/

public class BookTest {
	
	private Book testBook1;
	private User testUser1;
	private User testUser2;
	private User testUser3;
	private Library testLibrary1;
	
	//List<MyClass> classList = new ArrayList<MyClass>();
	
	@Before
	public void buildUp() {
		testBook1 = new LibraryBook("Charles Dickens", "Great Expectations");		
		
		testUser2 = new LibraryUser("David");
		testUser3 = new LibraryUser("Steve");
		
		testUser1 = new LibraryUser("Julian");
		testLibrary1 = new Library ("Birkbeck",808,4);
		testUser1.register(testLibrary1);
		
	}
	
	
	@Test
	public void testAuthor() {
		String expectedAuthor = "Charles Dickens";
		String outputAuthor = testBook1.getAuthor();
		assertEquals(expectedAuthor, outputAuthor);
	}
	
	@Test
	public void testTitle() {
		String expectedTitle = "Great Expectations";
		String outputTitle = testBook1.getTitle();
		assertEquals(expectedTitle, outputTitle);
	}
	
	@Test
	public void testUser_name() {
		String expectedName = "Julian";
		String outputName = testUser1.getName();
		assertEquals(expectedName, outputName);
	}
	
	@Test
	public void testLibrary_name() {
		String expectedName = "Birkbeck";
		String outputName = testLibrary1.getName();
		assertEquals(expectedName, outputName);
	}
	
	@Test
	public void testUser_register() {
		String expectedName = "Birkbeck";
		String outputName = testUser1.register(testLibrary1);
		assertEquals(expectedName, outputName);
	}
	
	@Test
	public void testUser_registerMultiple() {
		
		testUser2.register(testLibrary1);
		testUser3.register(testLibrary1);
		
		int expectedSize = 3;
		int outputSize = testLibrary1.countMembers();
		
		assertEquals(expectedSize, outputSize);
	}
	
	
	@Test
	public void testLibrary_getId() {
		int expectedId = 808;
		int outputId = testLibrary1.getLibraryId();
		assertEquals(expectedId, outputId);
	}
	
	
	@Test
	public void testLibrary_getName() {
		Library l = new Library("Jools Library", 909, 2);
		String expectedName = "Jools Library";
		String outputName = l.getName();
		assertEquals(expectedName, outputName);
	}
	
	@Test
	public void testLibrary_maxBookPolicy() {
		int expectedId = 4;
		int outputId = testLibrary1.getMaxBooksPerUser();
		assertEquals(expectedId, outputId);
	}
	
	@Test
	public void testUser_setId() {
		int expectedId = 1000;
		int outputId = testUser1.getId();
		assertEquals(expectedId, outputId);
	}
	
	@Test
	public void testLibrary_getUserId() {
		int expectedId = 1000;
		int outputId = testLibrary1.getId("Julian");
		assertEquals(expectedId, outputId);
	}
	
	@Test
	public void testLibrary_addBook() {
		String expectedTitle = "The Tempest";
		String outputTitle = testLibrary1.addBook("William Shakespere", "The Tempest");
		assertEquals(expectedTitle, outputTitle);
	}
	
	@Test 
	public void testLibrary_takeBook(){
		testLibrary1.addBook("William Shakespere", "The Tempest");
		testLibrary1.takeBook("The Tempest");
		boolean expectedFlag = false;
		boolean outputFlag = testLibrary1.takeBook("The Tempest");
		assertEquals(expectedFlag, outputFlag);
	}
	
	@Test
		public void testLibary_returnBook(){
		testLibrary1.addBook("William Shakespere", "The Tempest");
		testLibrary1.takeBook("The Tempest");
		
		String str = "The Tempest";
		
		for(LibraryBook next:testLibrary1.libraryBooksList){	
			if(next.getTitle().equals(str)){
				testLibrary1.returnBook(next);
			}
		}
		testLibrary1.takeBook("The Tempest");
		boolean expectedFlag = false;
		boolean outputFlag = testLibrary1.takeBook("The Tempest");
		assertEquals(expectedFlag, outputFlag);
		
		
		
		
	}
	
	
}