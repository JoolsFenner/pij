import java.util.ArrayList;

public class Library implements Lib{
	private String libraryName;
	private int libraryId;
	private int maxBookPolicy;
	private int IdToAllocate;
	private int nextIdToAllocate;
	private ArrayList<LibraryUser> libraryMembersList;
	public ArrayList<LibraryBook> libraryBooksList;
	
	public Library(String libraryName, int libraryId, int maxBookPolicy){
		this.libraryName = libraryName;
		this.libraryId = libraryId;
		this.maxBookPolicy = maxBookPolicy;
		libraryMembersList = new ArrayList<LibraryUser>();
		libraryBooksList = new ArrayList<LibraryBook>();
		nextIdToAllocate = 1000; //first memberShip number, increments from here.
	}
	
	//Library methods
	public String getName(){
		return libraryName;
	}
	
	public int getLibraryId(){
		return libraryId;
	}
	
	public int countMembers(){
		return libraryMembersList.size();
	}
	
	//Library user methods
	public int setNewUserId(){
		nextIdToAllocate++;
		return nextIdToAllocate-1;
	}
	
	public int getId(String str){
		int foundId = 0;
		
		for(LibraryUser next:libraryMembersList){	
			if(next.getName().equals(str)){
				foundId = next.getId();
				break;
			} 
		}
		
		if(foundId == 0){
			System.out.println("userNotFound");
		}
		
		return foundId;
	}
	
	public void addMember(LibraryUser user){
		libraryMembersList.add(user);
	}
	
	public int getMaxBooksPerUser(){
		return maxBookPolicy;
	}
	
	//BORROW BOOK METHOD
	
	public String addBook(String title, String author){
		LibraryBook tempBook = new LibraryBook(title, author);
		libraryBooksList.add(tempBook);
		return tempBook.getTitle();
	}
	
	public boolean takeBook (String title){
		boolean takeBook = false;
		
		for(LibraryBook next:libraryBooksList){	
			if(next.getTitle().equals(title)){
				if(next.getTaken()==true){
					takeBook = false;
					break;
				} else{
					next.setTaken(true);
					takeBook = true;
					break;
				}
			}
		}	
		
		return takeBook;
	}
	
	public void returnBook(Book book){
		book.setTaken(false);
	}
	
}