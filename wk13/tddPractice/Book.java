public interface Book{
	String getAuthor();
	String getTitle();
	public boolean getTaken();
	public void setTaken(boolean flag);
}