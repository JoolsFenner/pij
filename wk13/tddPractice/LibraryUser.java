public class LibraryUser implements User {
	private final String name;
	public Library libraryJoined;
	private int id;
	public LibraryUser(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	public void setId(){
		this.id = libraryJoined.setNewUserId();
		
	}
	
	public int getId(){
		return id;
	}
	
	public String register(Library libName){
		libraryJoined = libName;
		libraryJoined.addMember(this);
		setId();
		
		return libraryJoined.getName();
	}
	
	
}